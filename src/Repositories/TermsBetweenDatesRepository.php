<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Term;

class TermsBetweenDatesRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\TermsBetweenDatesRepository
{
    use RepositoryFromEloquent;

    public function __construct(protected Term $model) {}

    public function __invoke(
        Carbon|string|null $start,
        Carbon|string|null $end,
        int $perPage = 50
    ): Collection|Paginator {
        // @phpstan-ignore method.notFound
        $this->addQueryCallback(fn (Builder $query) => $query->betweenDates($start, $end));
        $query = $this->query();
        if ($perPage) {
            return $query->paginate($perPage, $this->getColumns());
        }

        return $query->get($this->getColumns());
    }

    protected function modifyQuery(Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->order()->credit();
    }
}
