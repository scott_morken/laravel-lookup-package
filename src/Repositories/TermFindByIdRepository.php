<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Lookup\Contracts\Models\Term;

class TermFindByIdRepository extends EloquentRetrieveRepository implements \Smorken\Lookup\Contracts\Repositories\TermFindByIdRepository
{
    public function __construct(Term $model)
    {
        parent::__construct($model);
    }
}
