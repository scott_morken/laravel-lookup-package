<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Hcm;

class PersonsHcmGetByBirthdateRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByBirthdateRepository
{
    use RepositoryFromEloquent;

    public function __construct(protected Hcm $model) {}

    public function __invoke(Carbon|string $birthdate, int $perPage = 50): iterable|Paginator|Collection
    {
        if (is_string($birthdate)) {
            $birthdate = Carbon::parse($birthdate)->startOfDay();
        }
        // HCM has no birthdate column
        if ($perPage) {
            return new \Illuminate\Pagination\Paginator([], $perPage);
        }

        return new Collection;
    }
}
