<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Person;

class PersonsSisGetByBirthdateRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\PersonsSisGetByBirthdateRepository
{
    use RepositoryFromEloquent;

    public function __construct(protected Person $model) {}

    public function __invoke(Carbon|string $birthdate, int $perPage = 50): iterable|Paginator|Collection
    {
        if (is_string($birthdate)) {
            $birthdate = Carbon::parse($birthdate)->startOfDay();
        }
        // @phpstan-ignore method.notFound
        $this->addQueryCallback(fn (Builder $query) => $query->birthdateIs($birthdate));
        $query = $this->query();

        if ($perPage) {
            return $query->paginate($perPage, $this->getColumns());
        }

        return $query->get($this->getColumns());
    }
}
