<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Klass;
use Smorken\Sis\Enums\Relations;

class ClassFindByTermIdAndClassNumberRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\ClassFindByTermIdAndClassNumberRepository
{
    use RepositoryFromEloquent;

    protected CacheType $cacheType = CacheType::BOTH;

    public function __construct(protected Klass $model) {}

    public function __invoke(string $termId, string $classNumber, bool $throw = true): ?Klass
    {
        return $this->getCacheHandler()->remember(
            $termId.$classNumber,
            $this->getDefaultCacheTTL(),
            fn () => $this->retrieveAndAuthorize($termId, $classNumber, $throw)
        );
    }

    protected function retrieve(string $termId, string $classNumber, bool $throw = true): ?Klass
    {
        $this->addQueryCallback(fn (Builder $query) => $query->select($this->getColumns()));
        $query = $this->query();
        // @phpstan-ignore method.notFound
        $query->termIdIs($termId)
            ->classNumberIs($classNumber)
            ->with([Relations::ENROLLMENTS, Relations::CLASS_INSTRUCTORS]);

        /** @var ?Klass */
        return $throw ? $query->firstOrFail() : $query->first();
    }

    protected function retrieveAndAuthorize(string $termId, string $classNumber, bool $throw = true): ?Klass
    {
        $model = $this->retrieve($termId, $classNumber, $throw);
        if ($model) {
            $this->authorize($model, PolicyType::VIEW);
        }

        return $model;
    }
}
