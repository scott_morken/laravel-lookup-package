<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Term;

class TermFindActiveOrNextRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\TermFindActiveOrNextRepository
{
    use RepositoryFromEloquent;

    protected mixed $cacheKey = 'activeOrNext';

    protected CacheType $cacheType = CacheType::BOTH;

    public function __construct(protected Term $model) {}

    public function __invoke(): Term
    {
        return $this->getCacheHandler()->remember(
            $this->getCacheKey(),
            $this->getDefaultCacheTTL(),
            fn () => $this->findActiveOrNextTerm()
        );
    }

    protected function findActiveOrNextTerm(): Term
    {
        $query = $this->query();

        /** @var Term */
        return $query->first($this->getColumns());
    }

    protected function modifyQuery(Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->order()->activeAndUpcoming();
    }
}
