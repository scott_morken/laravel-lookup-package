<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Repositories\Concerns\HasPageName;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Klass;

class ClassesByClassNameAndTermIdRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\ClassesByClassNameAndTermIdRepository
{
    use HasPageName, RepositoryFromEloquent;

    protected string $pageName = 'page';

    public function __construct(protected Klass $model) {}

    public function __invoke(string $className, string $termId, int $perPage = 20): Paginator|Collection
    {
        $this->authorize($this->model(), PolicyType::VIEW_ANY);

        return $this->getIterable($className, $termId, $perPage);
    }

    protected function getIterable(
        string $className,
        string $termId,
        int $perPage
    ): iterable|Paginator|Collection {
        // @phpstan-ignore method.notFound
        $query = $this->query()
            ->classNameLike($className)
            ->termIdIs($termId)
            ->orderByName();
        if ($perPage) {
            return $this->modifyPaginator($query->paginate($perPage, $this->getColumns(), $this->getPageName()));
        }

        return $this->modifyCollection($query->get($this->getColumns()));
    }

    protected function modifyCollection(Collection $models): Collection
    {
        return $models;
    }

    protected function modifyPaginator(Paginator $paginator): Paginator
    {
        return $paginator;
    }
}
