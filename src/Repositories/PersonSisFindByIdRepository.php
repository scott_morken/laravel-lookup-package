<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Lookup\Contracts\Models\Person;

class PersonSisFindByIdRepository extends EloquentRetrieveRepository implements \Smorken\Lookup\Contracts\Repositories\PersonSisFindByIdRepository
{
    public function __construct(Person $model)
    {
        parent::__construct($model);
    }
}
