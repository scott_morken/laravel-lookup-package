<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\EloquentIterableRepository;
use Smorken\Lookup\Contracts\Models\Term;

class TermsAllRepository extends EloquentIterableRepository implements \Smorken\Lookup\Contracts\Repositories\TermsAllRepository
{
    public function __construct(Term $model)
    {
        parent::__construct($model);
    }

    protected function modifyCollection(Collection $models): Collection
    {
        return $models->keyBy('id')->values();
    }

    protected function modifyQuery(Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->order(0)->credit();
    }
}
