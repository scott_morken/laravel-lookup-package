<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Lookup\Contracts\Models\Hcm;

class PersonHcmFindByIdRepository extends EloquentRetrieveRepository implements \Smorken\Lookup\Contracts\Repositories\PersonHcmFindByIdRepository
{
    public function __construct(Hcm $model)
    {
        parent::__construct($model);
    }

    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): ?Model
    {
        $id = (int) $id;
        // @phpstan-ignore method.notFound
        $query = $query->studentIdIs($id);

        return $throw ? $query->firstOrFail() : $query->first();
    }
}
