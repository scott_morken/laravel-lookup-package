<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\Lookup\Contracts\Models\Hcm;

class PersonsHcmGetByFirstLastNameRepository extends Repository implements \Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByFirstLastNameRepository
{
    use RepositoryFromEloquent;

    public function __construct(protected Hcm $model) {}

    public function __invoke(?string $firstName, ?string $lastName, int $perPage = 50): Collection|Paginator
    {
        // @phpstan-ignore method.notFound
        $this->addQueryCallback(fn (Builder $query) => $query->nameLike($lastName, $firstName));
        $query = $this->query();
        if ($perPage) {
            return $query->paginate($perPage, $this->getColumns());
        }

        return $query->get($this->getColumns());
    }
}
