<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Lookup\Contracts\Models\Person;

class PersonSisFindByAltIdRepository extends EloquentRetrieveRepository implements \Smorken\Lookup\Contracts\Repositories\PersonSisFindByAltIdRepository
{
    public function __construct(Person $model)
    {
        parent::__construct($model);
    }

    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): ?Model
    {
        // @phpstan-ignore method.notFound
        $query = $query->altIdIs($id);

        return $throw ? $query->firstOrFail() : $query->first();
    }
}
