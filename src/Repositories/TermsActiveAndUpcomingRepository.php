<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\EloquentIterableRepository;
use Smorken\Lookup\Contracts\Models\Term;

class TermsActiveAndUpcomingRepository extends EloquentIterableRepository implements \Smorken\Lookup\Contracts\Repositories\TermsActiveAndUpcomingRepository
{
    protected mixed $cacheKey = 'activeUpcoming';

    protected CacheType $cacheType = CacheType::BOTH;

    public function __construct(Term $model)
    {
        parent::__construct($model);
    }

    protected function modifyQuery(Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->activeAndUpcoming()->credit()->order();
    }
}
