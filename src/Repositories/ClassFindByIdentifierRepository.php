<?php

declare(strict_types=1);

namespace Smorken\Lookup\Repositories;

use Smorken\Domain\Repositories\RetrieveRepository;
use Smorken\Lookup\Contracts\Models\Klass;
use Smorken\Lookup\Contracts\Repositories\ClassFindByTermIdAndClassNumberRepository as FindRepository;
use Smorken\Lookup\Support\ClassIdentifier;

class ClassFindByIdentifierRepository extends RetrieveRepository implements \Smorken\Lookup\Contracts\Repositories\ClassFindByIdentifierRepository
{
    public function __construct(protected FindRepository $findRepository) {}

    protected function retrieve(mixed $id, bool $throw): ?Klass
    {
        $identifier = new ClassIdentifier($id);

        return ($this->findRepository)($identifier->termId, $identifier->classNumber, $throw);
    }
}
