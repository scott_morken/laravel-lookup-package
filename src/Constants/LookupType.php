<?php

declare(strict_types=1);

namespace Smorken\Lookup\Constants;

enum LookupType: string
{
    case PERSON = 'person';
    case KLASS = 'class';

    case TERM = 'term';

    public static function toArray(): array
    {
        return array_map(fn (LookupType $type) => [$type->value => $type->text()], self::cases());
    }

    public function text(): string
    {
        return match ($this) {
            self::PERSON => 'Person',
            self::KLASS => 'Class',
            self::TERM => 'Term',
        };
    }
}
