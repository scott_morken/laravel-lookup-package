<?php

declare(strict_types=1);

namespace Smorken\Lookup\Constants;

enum PersonType: string
{
    case SIS = 'sis';
    case HCM = 'hcm';
}
