<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:00 AM
 */

namespace Smorken\Lookup\Lookups\WithProvider;

use Illuminate\Support\Arr;
use Smorken\Lookup\Contracts\Lookups\WithProvider;
use Smorken\Lookup\Contracts\Storage\LookupBase;

/**
 * @deprecated
 */
abstract class Base implements \Smorken\Lookup\Contracts\Lookups\Base, WithProvider
{
    protected array $providers = [];

    public function __construct(array $providers)
    {
        foreach ($providers as $key => $provider) {
            $this->setProvider($key, $provider);
        }
    }

    public function getProvider(string $key): LookupBase
    {
        if (array_key_exists($key, $this->providers)) {
            return Arr::get($this->providers, $key);
        }
        throw new \InvalidArgumentException("$key is not a valid provider.");
    }

    public function setProvider(string $key, LookupBase $provider): void
    {
        $this->providers[$key] = $provider;
    }
}
