<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:59 AM
 */

namespace Smorken\Lookup\Lookups\WithProvider;

use Illuminate\Contracts\Pagination\Paginator;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
class Term extends Base implements \Smorken\Lookup\Contracts\Lookups\Term
{
    public function byId(string $id): ?Model
    {
        return $this->getProvider('term')
            ->findById($id);
    }

    /**
     * @param  mixed  $identifier
     */
    public function find(array|string|int $identifier): Model|Paginator|\Illuminate\Support\Collection|null
    {
        return $this->byId($identifier);
    }
}
