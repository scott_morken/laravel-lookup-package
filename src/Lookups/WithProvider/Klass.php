<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:02 AM
 */

namespace Smorken\Lookup\Lookups\WithProvider;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
class Klass extends Base implements \Smorken\Lookup\Contracts\Lookups\Klass
{
    public function byId(string $id): ?Model
    {
        return $this->getProvider('klass')
            ->findById($id);
    }

    /**
     * @param  mixed  $identifier
     */
    public function find(array|string|int $identifier): Model|Paginator|Collection|null
    {
        [$i, $t] = $identifier;
        $m = $this->findByKlassNumberAndTerm($i, $t);
        if (! $m) {
            $m = $this->findBySubjectCatNumberAndTerm($i, $t);
        }

        return $m;
    }

    protected function findByKlassNumberAndTerm(
        string $identifier,
        string $term
    ): ?\Smorken\Lookup\Contracts\Models\Klass {
        if (strlen($identifier) === 5 && preg_match('/[0-9]{5}/', $identifier)) {
            return $this->getProvider('klass')
                ->findByKlassNumberAndTerm($identifier, $term);
        }

        return null;
    }

    protected function findBySubjectCatNumberAndTerm(string $identifier, string $term): Collection
    {
        return $this->getProvider('klass')
            ->findBySubjectCatNumberAndTerm($identifier, $term);
    }
}
