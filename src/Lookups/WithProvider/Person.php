<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:20 AM
 */

namespace Smorken\Lookup\Lookups\WithProvider;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Collection;
use Smorken\Lookup\Contracts\Models\Hcm;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
class Person extends Base implements \Smorken\Lookup\Contracts\Lookups\Person
{
    public function byId(string $id): ?Model
    {
        if (! $this->identifierIsInt($id)) {
            return null;
        }
        $m = $this->findById($id);
        if (! $m) {
            $m = $this->findById($id, 'hcm');
        }

        return $m;
    }

    /**
     * @param  mixed  $identifier
     */
    public function find(array|string|int $identifier): Model|Paginator|Collection|null
    {
        $m = $this->findByLastFirst($identifier);
        if (! $m && $this->identifierIsMeid($identifier)) {
            $m = $this->findByAltId($identifier);
        }
        if (! $m && $this->identifierIsInt($identifier)) {
            $m = $this->findById($identifier);
        }
        if (! $m || $m instanceof Collection || $m instanceof AbstractPaginator) {
            $m = $this->tryHcm($identifier, $m);
        }

        return $m;
    }

    protected function convertToPerson(\Smorken\Lookup\Contracts\Models\Person|Hcm|null $model
    ): ?\Smorken\Lookup\Contracts\Models\Person {
        if ($model instanceof Hcm) {
            return $model->asModel();
        }

        return $model;
    }

    protected function findByAltId(string $identifier, $type = 'person'): ?\Smorken\Lookup\Contracts\Models\Person
    {
        return $this->convertToPerson($this->getProvider($type)
            ->findByAltId($identifier));
    }

    protected function findById(int $identifier, $type = 'person'): ?\Smorken\Lookup\Contracts\Models\Person
    {
        return $this->convertToPerson($this->getProvider($type)
            ->findById($identifier));
    }

    protected function findByLastFirst(array|string $identifier, $type = 'person'): Collection|AbstractPaginator|null
    {
        if (is_array($identifier)) {
            [$last, $first] = $identifier;

            return $this->getProvider($type)
                ->findByLastFirst($last, $first);
        }

        return null;
    }

    protected function getCollectionFromResults($results): Collection|Paginator
    {
        if ($results instanceof Collection) {
            return $results;
        }
        if ($results instanceof Paginator) {
            return $results;
        }

        return new Collection;
    }

    protected function identifierIsInt(array|string $identifier): bool
    {
        return ! is_array($identifier) && filter_var($identifier, FILTER_VALIDATE_INT);
    }

    protected function identifierIsMeid(array|string $identifier): bool
    {
        return ! is_array($identifier) && preg_match('([A-z]{3}[0-9]{7}|[A-z]{5}[0-9]{5})', $identifier);
    }

    protected function mergeResults(
        Collection|Paginator|null $hcm,
        Collection|Paginator|null $sis
    ): Collection|Paginator {
        $hcm = $this->getCollectionFromResults($hcm);
        $sis = $this->getCollectionFromResults($sis);
        if (count($hcm) > 0) {
            $hcm = $hcm->keyBy(
                fn ($c) => 'k'.$c->getModelId()
            );
            $sis = $sis->keyBy(
                fn ($c) => 'k'.$c->getModelId()
            );
            foreach ($hcm as $key => $model) {
                if (! $sis->has($key)) {
                    $sis->put($key, $this->convertToPerson($model));
                }
            }
            $ordered = $sis->sortBy(
                fn ($c) => $c->last_name
            )
                ->sortBy(
                    fn ($c) => $c->first_name
                );

            return $ordered->slice(0, 50);
        }

        return $sis;
    }

    protected function tryHcm(array|string|int $identifier, Collection|Paginator|null $original)
    {
        $m = $this->mergeResults($this->findByLastFirst($identifier, 'hcm'), $original);
        if ($m->count() > 0) {
            return $m;
        }
        if ($this->identifierIsMeid($identifier)) {
            return $this->findByAltId($identifier, 'hcm');
        }
        if ($this->identifierIsInt($identifier)) {
            return $this->findById($identifier, 'hcm');
        }

        return null;
    }
}
