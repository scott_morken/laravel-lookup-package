<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:49 AM
 */

namespace Smorken\Lookup\Storage\Eloquent;

use Illuminate\Support\Collection;

/**
 * @deprecated
 */
class Klass extends Base implements \Smorken\Lookup\Contracts\Storage\Klass
{
    /**
     * @return \Smorken\Model\Contracts\Model|null
     */
    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Klass
    {
        [$cn, $t] = $this->splitKlassNumberTerm($id);

        return $this->findByKlassNumberAndTerm($cn, $t);
    }

    public function findByKlassNumberAndTerm(
        string $klass_number,
        string $term
    ): ?\Smorken\Lookup\Contracts\Models\Klass {
        return $this->getCacheAssist()->remember(['tcn', $term.$klass_number],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            fn () => $this->getModel()
                ->newQuery()
                ->classNumberIs($klass_number)
                ->termIdIs($term)
                ->first());
    }

    public function findBySubjectCatNumberAndTerm(string $identifier, string $term): Collection
    {
        return $this->getModel()
            ->newQuery()
            ->termIdIs($term)
            ->with('term')
            ->subjectCatNumberLike($identifier)
            ->orderClassNameNumber()
            ->limit(50)
            ->get();
    }

    protected function splitKlassNumberTerm(string $id): array
    {
        $t = substr($id, 0, 4);
        $kn = substr($id, 4);

        return [$kn, $t];
    }
}
