<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:57 AM
 */

namespace Smorken\Lookup\Storage\Eloquent;

use Carbon\Carbon;

/**
 * @deprecated
 */
class Term extends Base implements \Smorken\Lookup\Contracts\Storage\Term
{
    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Term
    {
        return $this->getCacheAssist()->remember([$id],
            Carbon::now()->addHour(),
            fn () => $this->getModel()
                ->newQuery()
                ->termIdIs($id)
                ->first());
    }
}
