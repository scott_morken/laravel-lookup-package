<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:44 AM
 */

namespace Smorken\Lookup\Storage\Eloquent;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * @deprecated
 */
class Hcm extends Base implements \Smorken\Lookup\Contracts\Storage\Hcm
{
    public function findByAltId(string $alt_id): ?\Smorken\Lookup\Contracts\Models\Hcm
    {
        return $this->getCacheAssist()->remember(['alt_id', $alt_id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            fn () => $this->getModel()
                ->newQuery()
                ->altIdIs($alt_id)
                ->first());
    }

    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Hcm
    {
        return $this->getCacheAssist()->remember(['id', $id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            fn () => $this->getModel()
                ->newQuery()
                ->idIs($id)
                ->first());
    }

    public function findByLastFirst(?string $last, ?string $first): Collection|Paginator|null
    {
        return $this->getModel()
            ->newQuery()
            ->nameLike($last, $first)
            ->orderLastFirst()
            ->paginate(50);
    }
}
