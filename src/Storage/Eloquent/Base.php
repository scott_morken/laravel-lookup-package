<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:44 AM
 */

namespace Smorken\Lookup\Storage\Eloquent;

use Smorken\Lookup\Contracts\Storage\LookupBase;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
abstract class Base implements LookupBase
{
    use \Smorken\Storage\Traits\CacheAssist;

    public function __construct(protected Model $model, array $cacheOptions = [])
    {
        if ($cacheOptions) {
            $this->cacheAssist = $this->createCacheAssist($cacheOptions);
        }
    }

    public function getModel(): Model
    {
        return $this->model;
    }
}
