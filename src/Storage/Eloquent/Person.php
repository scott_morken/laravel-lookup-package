<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:54 AM
 */

namespace Smorken\Lookup\Storage\Eloquent;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * @deprecated
 */
class Person extends Base implements \Smorken\Lookup\Contracts\Storage\Person
{
    public function findByAltId(string $alt_id): ?\Smorken\Lookup\Contracts\Models\Person
    {
        return $this->getCacheAssist()->remember(['alt_id', $alt_id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            fn () => $this->getModel()
                ->newQuery()
                ->with('hcm')
                ->meidIs($alt_id)
                ->first());
    }

    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Person
    {
        return $this->getCacheAssist()->remember(['id', $id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            fn () => $this->getModel()
                ->newQuery()
                ->with('hcm')
                ->emplidIs($id)
                ->first());
    }

    public function findByLastFirst(?string $last, ?string $first): Collection|Paginator|null
    {
        return $this->getModel()
            ->newQuery()
            ->with('hcm')
            ->nameLike($last, $first)
            ->orderLastFirst()
            ->paginate(50);
    }
}
