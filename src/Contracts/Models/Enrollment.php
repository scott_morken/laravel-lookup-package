<?php

namespace Smorken\Lookup\Contracts\Models;

interface Enrollment extends \Smorken\Sis\Contracts\Base\Student\Enrollment, Lookup {}
