<?php

namespace Smorken\Lookup\Contracts\Models;

interface KlassInstructor extends \Smorken\Sis\Contracts\Base\Klass\ClassInstructor, Lookup {}
