<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:28 AM
 */

namespace Smorken\Lookup\Contracts\Models;

/**
 * @property \Smorken\Lookup\Constants\LookupType $type
 * @property \Illuminate\Support\Collection|\Illuminate\Pagination\AbstractPaginator|\Smorken\Lookup\Contracts\Models\Lookup|null $result
 */
interface Result
{
    public function hasResult(): bool;

    public function isCollection(): bool;

    public function isModel(): bool;
}
