<?php

namespace Smorken\Lookup\Contracts\Models;

interface Meeting extends \Smorken\Sis\Contracts\Base\Klass\Meeting, Lookup {}
