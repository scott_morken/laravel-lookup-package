<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:27 AM
 */

namespace Smorken\Lookup\Contracts\Models;

use Smorken\Model\Contracts\Model;

interface Lookup
{
    public function asModel(): Model;

    public function getLongName(): string;

    public function getModelId(): string|int;

    public function getShortName(): string;
}
