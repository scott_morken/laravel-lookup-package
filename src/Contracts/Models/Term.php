<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:39 AM
 */

namespace Smorken\Lookup\Contracts\Models;

interface Term extends \Smorken\Sis\Contracts\Base\Term, Lookup {}
