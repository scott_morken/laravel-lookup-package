<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:14 AM
 */

namespace Smorken\Lookup\Contracts\Models;

interface Hcm extends \Smorken\Hrms\Contracts\Models\Hcm, Lookup
{
    public function isEmployee(): bool;
}
