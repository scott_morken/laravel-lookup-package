<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:38 AM
 */

namespace Smorken\Lookup\Contracts\Models;

/**
 * @property \Smorken\Lookup\Contracts\Models\Hcm $hcm
 * @property \Illuminate\Support\Collection $enrollments
 * @property \Illuminate\Support\Collection $instructsClasses
 */
interface Person extends \Smorken\Sis\Contracts\Base\Person\Person, Lookup
{
    public function __construct(array $attributes = []);

    public function isAdjunct(): bool;

    public function isEmployee(): bool;

    public function isFaculty(): bool;

    public function isResidential(): bool;

    public static function fromHcmModel(Hcm $hcm): static;
}
