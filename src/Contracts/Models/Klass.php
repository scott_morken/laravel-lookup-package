<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:38 AM
 */

namespace Smorken\Lookup\Contracts\Models;

interface Klass extends \Smorken\Sis\Contracts\Base\Klass\Klass, Lookup {}
