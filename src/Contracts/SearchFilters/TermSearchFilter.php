<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\SearchFilters;

use Carbon\Carbon;

/**
 * @property-read ?string $id
 * @property-read string|\Carbon\Carbon|null $startDate
 * @property-read string|\Carbon\Carbon|null $endDate
 *
 * @phpstan-require-extends \Smorken\Lookup\SearchFilters\TermSearchFilter
 */
interface TermSearchFilter extends SearchFilter
{
    public function __construct(
        ?string $id,
        string|Carbon|null $startDate,
        string|Carbon|null $endDate
    );
}
