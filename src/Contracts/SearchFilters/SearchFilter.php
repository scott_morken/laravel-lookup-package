<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\SearchFilters;

use Illuminate\Http\Request;

interface SearchFilter
{
    public function is(): bool;

    public static function fromRequest(Request $request): static;
}
