<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\SearchFilters;

/**
 * @property-read ?string $id
 * @property-read ?string $termId
 * @property-read ?string $className
 * @property-read ?string $classNumber
 *
 * @phpstan-require-extends \Smorken\Lookup\SearchFilters\ClassSearchFilter
 */
interface ClassSearchFilter extends SearchFilter
{
    public function __construct(
        ?string $id,
        ?string $termId,
        ?string $className,
        ?string $classNumber
    );
}
