<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\SearchFilters;

/**
 * @property-read string|int|null $id
 * @property-read string|null $altId
 * @property-read string|null $firstName
 * @property-read string|null $lastName
 * @property-read string|null $birthdate
 *
 * @phpstan-require-extends \Smorken\Lookup\SearchFilters\PersonSearchFilter
 */
interface PersonSearchFilter extends SearchFilter
{
    public function __construct(
        string|int|null $id,
        ?string $altId,
        ?string $lastName,
        ?string $firstName,
        ?string $birthdate
    );
}
