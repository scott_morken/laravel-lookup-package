<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;

interface TermsBetweenDatesRepository extends Repository
{
    public function __invoke(
        Carbon|string|null $start,
        Carbon|string|null $end,
        int $perPage = 50
    ): Collection|Paginator;
}
