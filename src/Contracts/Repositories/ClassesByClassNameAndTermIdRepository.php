<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;

interface ClassesByClassNameAndTermIdRepository extends Repository
{
    public function __invoke(string $className, string $termId, int $perPage = 20): Paginator|Collection;
}
