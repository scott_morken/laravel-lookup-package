<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\Repository;
use Smorken\Lookup\Contracts\Models\Klass;

interface ClassFindByTermIdAndClassNumberRepository extends Repository
{
    public function __invoke(string $termId, string $classNumber, bool $throw = true): ?Klass;
}
