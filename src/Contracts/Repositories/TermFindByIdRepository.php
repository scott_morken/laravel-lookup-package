<?php

namespace Smorken\Lookup\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface TermFindByIdRepository extends RetrieveRepository {}
