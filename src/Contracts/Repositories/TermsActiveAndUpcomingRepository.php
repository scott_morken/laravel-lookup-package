<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\IterableRepository;

interface TermsActiveAndUpcomingRepository extends IterableRepository {}
