<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface PersonHcmFindByIdRepository extends RetrieveRepository {}
