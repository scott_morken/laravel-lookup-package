<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;

interface PersonsHcmGetByBirthdateRepository extends Repository
{
    public function __invoke(string|Carbon $birthdate, int $perPage = 50): iterable|Paginator|Collection;
}
