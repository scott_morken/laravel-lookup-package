<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;

interface PersonsHcmGetByFirstLastNameRepository extends Repository
{
    public function __invoke(?string $firstName, ?string $lastName, int $perPage = 50): Collection|Paginator;
}
