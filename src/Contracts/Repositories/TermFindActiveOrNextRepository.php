<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\Repository;
use Smorken\Lookup\Contracts\Models\Term;

interface TermFindActiveOrNextRepository extends Repository
{
    public function __invoke(): Term;
}
