<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:37 AM
 */

namespace Smorken\Lookup\Contracts\Storage;

/**
 * @deprecated
 */
interface Term extends LookupBase
{
    /**
     * @return \Smorken\Model\Contracts\Model|null
     */
    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Term;
}
