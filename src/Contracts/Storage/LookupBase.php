<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:19 AM
 */

namespace Smorken\Lookup\Contracts\Storage;

use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
interface LookupBase extends \Smorken\Storage\Contracts\CacheAssist
{
    public function getModel(): Model;
}
