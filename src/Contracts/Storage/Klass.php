<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:36 AM
 */

namespace Smorken\Lookup\Contracts\Storage;

use Illuminate\Support\Collection;

/**
 * @deprecated
 */
interface Klass extends LookupBase
{
    public function findByKlassNumberAndTerm(
        string $klass_number,
        string $term
    ): ?\Smorken\Lookup\Contracts\Models\Klass;

    public function findBySubjectCatNumberAndTerm(string $identifier, string $term): Collection;
}
