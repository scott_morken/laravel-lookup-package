<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:37 AM
 */

namespace Smorken\Lookup\Contracts\Storage;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * @deprecated
 */
interface Person extends LookupBase
{
    public function findByAltId(string $alt_id): ?\Smorken\Lookup\Contracts\Models\Person;

    /**
     * @return \Smorken\Model\Contracts\Model|null
     */
    public function findById(string $id): ?\Smorken\Lookup\Contracts\Models\Person;

    public function findByLastFirst(?string $last, ?string $first): Collection|Paginator|null;
}
