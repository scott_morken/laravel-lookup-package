<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Factories;

use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;

interface LookupFactory
{
    public function search(SearchFilter $filter): mixed;
}
