<?php

declare(strict_types=1);

namespace Smorken\Lookup\Contracts\Lookups;

use Smorken\Lookup\Contracts\Storage\LookupBase;

/**
 * @deprecated
 */
interface WithProvider
{
    public function getProvider(string $key): LookupBase;

    public function setProvider(string $key, LookupBase $provider): void;
}
