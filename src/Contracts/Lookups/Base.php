<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 3/29/18
 * Time: 9:37 AM
 */

namespace Smorken\Lookup\Contracts\Lookups;

use Illuminate\Contracts\Pagination\Paginator;
use Smorken\Model\Contracts\Model;

interface Base
{
    public function byId(string $id): ?Model;

    public function find(array|string|int $identifier): Model|Paginator|\Illuminate\Support\Collection|null;
}
