<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:18 AM
 */

namespace Smorken\Lookup\Contracts;

use Smorken\Lookup\Contracts\Lookups\Base;
use Smorken\Lookup\Contracts\Models\Result;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
interface Lookup
{
    public function byId(string $type, string $id): ?Model;

    public function getLookup(string $type): Base;

    public function lookup(array $filter): Result;

    public function setLookup(string $type, Base $provider): void;

    public function shouldLookup(array $filter): bool;
}
