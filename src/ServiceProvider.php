<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:15 AM
 */

namespace Smorken\Lookup;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Constants\PersonType;
use Smorken\Lookup\Factories\ClassRepositoryFactory;
use Smorken\Lookup\Factories\PersonFactory;
use Smorken\Lookup\Factories\PersonHcmRepositoryFactory;
use Smorken\Lookup\Factories\PersonSisRepositoryFactory;
use Smorken\Lookup\Factories\TermRepositoryFactory;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(
            \Smorken\Lookup\Contracts\Lookup::class,
            function ($app) {
                $lookups = $this->buildLookups($app);

                return new Lookup($lookups);
            }
        );
        $this->registerRepositories();
        $this->registerModels();
        $this->registerFactory();
        $this->registerPersonFactory();
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'lookup');
        $this->publishes([$config => config_path('lookup.php')], 'config');
    }

    protected function buildLookup(array $lookup): object
    {
        $providers = $this->buildProviders($lookup['providers'] ?? []);
        $impl = $lookup['impl'] ?? null;
        if (! $impl) {
            throw new \OutOfBoundsException('Implementation must be set on lookup.');
        }

        return new $impl($providers);
    }

    protected function buildLookups($app): array
    {
        $lookups = [];
        foreach ($app['config']->get('lookup', []) as $key => $lookup) {
            $lookups[$key] = $this->buildLookup($lookup);
        }

        return $lookups;
    }

    protected function buildProvider(array $provider): object
    {
        $impl = $this->getStorageClass($provider);
        $mclass = $this->getModelClass($provider);
        $m = new $mclass;
        $cao = $provider['cacheAssistOptions'] ?? [];

        return new $impl($m, $cao);
    }

    protected function buildProviders(array $providers): array
    {
        $built = [];
        foreach ($providers as $key => $provider) {
            $built[$key] = $this->buildProvider($provider);
        }

        return $built;
    }

    protected function getModelClass(array $provider): string
    {
        $mclass = $provider['model'] ?? null;
        if (! $mclass) {
            throw new \OutOfBoundsException('Model must be set on provider.');
        }

        return $mclass;
    }

    protected function getStorageClass(array $provider): string
    {
        $impl = $provider['impl'] ?? null;
        if (! $impl) {
            throw new \OutOfBoundsException('Implementation must be set on provider.');
        }

        return $impl;
    }

    protected function registerFactory(): void
    {
        $this->app->scoped(Factory::class, fn (Application $app) => new Factory([
            LookupType::KLASS->value => $app[ClassRepositoryFactory::class],
            LookupType::PERSON->value => $app[PersonFactory::class],
            LookupType::TERM->value => $app[TermRepositoryFactory::class],
        ]));
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('lookup.models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerPersonFactory(): void
    {
        $this->app->scoped(PersonFactory::class, fn (Application $app) => new PersonFactory([
            PersonType::SIS->value => $app[PersonSisRepositoryFactory::class],
            PersonType::HCM->value => $app[PersonHcmRepositoryFactory::class],
        ]));
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('lookup.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
