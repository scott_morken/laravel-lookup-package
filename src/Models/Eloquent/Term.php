<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:40 AM
 */

namespace Smorken\Lookup\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\TermAttributeMap;

#[MapAttributeNames(new TermAttributeMap)]
class Term extends \Smorken\Sis\Db\Core\Term implements \Smorken\Lookup\Contracts\Models\Term
{
    public function asModel(): Model
    {
        return $this;
    }

    public function getLongName(): string
    {
        return $this->getShortName();
    }

    public function getModelId(): string|int
    {
        return $this->STRM;
    }

    public function getShortName(): string
    {
        return $this->description;
    }

    public function scopeBetweenDates(Builder $query, Carbon|string|null $start, Carbon|string|null $end): Builder
    {
        if ($start) {
            // @phpstan-ignore method.notFound
            $query->onOrAfterDate($start);
        }
        if ($end) {
            // @phpstan-ignore method.notFound
            $query->onOrBeforeDate($end);
        }

        return $query;
    }
}
