<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:35 AM
 */

namespace Smorken\Lookup\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\PersonAttributeMap;
use Smorken\Sis\Enums\Relations;

#[MapAttributeNames(new PersonAttributeMap)]
class Person extends \Smorken\Sis\Db\Incremental\Person\Person implements \Smorken\Lookup\Contracts\Models\Person
{
    protected array $relationMap = [
        Relations::INSTRUCTS_CLASSES => KlassInstructor::class,
        Relations::ENROLLMENTS => Enrollment::class,
    ];

    public static function fromHcmModel(\Smorken\Lookup\Contracts\Models\Hcm $hcm): static
    {
        $attributeMap = [
            'PERS_EMPLID' => 'CAMPUS_ID',
            'PERS_OPRID' => 'MC_MEID',
            'PERS_PRIMARY_FIRST_NAME' => 'preferred_first_name',
            'PERS_PRIMARY_LAST_NAME' => 'preferred_last_name',
            'PERS_EMAIL_ADDR' => 'MC_BUS_EMAIL',
            'PERS_HOME_PHONE_NBR' => 'MC_BUS_PHONE',
            'HRMS_EMPLID' => 'MC_HCM_EMPLID',
        ];
        $attributes = [];
        foreach ($attributeMap as $personAttribute => $hcmAttribute) {
            $value = $hcm->getAttribute($hcmAttribute);
            $attributes[$personAttribute] = $value;
        }

        $m = (new static)->forceFill($attributes);
        $m->setRelation('hcm', $hcm);

        return $m;
    }

    /**
     * @return \Smorken\Lookup\Contracts\Models\Person
     */
    public function asModel(): Model
    {
        return $this;
    }

    public function getEmailAttribute(): ?string
    {
        if ($this->hcm && $this->hcm->email) {
            return $this->hcm->email;
        }

        return $this->attributes['PERS_EMAIL_ADDR'] ?? null;
    }

    public function getJobsAttribute(): Collection
    {
        if ($this->isEmployee()) {
            return $this->hcm->jobs;
        }

        return new Collection;
    }

    public function getLongName(): string
    {
        return sprintf(
            '%s &middot; %s &middot; Emp:%s/Fac:%s',
            $this->getShortName(),
            $this->altId,
            $this->isEmployee() ? 'Y' : 'N',
            $this->isFaculty() ? 'Y' : 'N'
        );
    }

    public function getModelId(): string|int
    {
        return $this->PERS_EMPLID;
    }

    public function getPhoneAttribute(): ?string
    {
        if ($this->hcm && $this->hcm->phone) {
            return $this->hcm->phone;
        }

        return $this->attributes['PERS_HOME_PHONE_NBR'] ?? null;
    }

    public function getShortName(): string
    {
        return $this->fullName();
    }

    public function hcm(): BelongsTo
    {
        return $this->belongsTo(Hcm::class, 'PERS_EMPLID', 'CAMPUS_ID');
    }

    public function isAdjunct(): bool
    {
        return $this->hcm && $this->hcm->isAdjunct();
    }

    public function isEmployee(): bool
    {
        return $this->hcm && $this->hcm->isEmployee();
    }

    public function isFaculty(): bool
    {
        return $this->hcm && $this->hcm->isFaculty();
    }

    public function isResidential(): bool
    {
        return $this->hcm && $this->hcm->isResidential();
    }

    public function scopeBirthdateIs(Builder $query, Carbon $date): Builder
    {
        return $query->where('PERS_BIRTHDATE', '=', $date->format('Y-m-d'));
    }

    public function scopeNameLike(Builder $query, ?string $last, ?string $first): Builder
    {
        if ($last) {
            $last = $last.'%';
            $query = $query->where('PERS_PRIMARY_LAST_NAME', 'LIKE', $last);
        }
        if ($first) {
            $first = $first.'%';
            $query->where(
                function ($q) use ($first) {
                    $q->where('PERS_PRIMARY_FIRST_NAME', 'LIKE', $first)
                        ->orWhere('PERS_PREFERRED_FIRST_NAME', 'LIKE', $first);
                }
            );
        }

        return $query;
    }

    public function scopeOrderLastFirst(Builder $query): Builder
    {
        return $query->orderBy('PERS_PRIMARY_LAST_NAME')
            ->orderBy('PERS_PRIMARY_FIRST_NAME');
    }
}
