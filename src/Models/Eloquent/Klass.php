<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 8:31 AM
 */

namespace Smorken\Lookup\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\ClassAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\ClassModifiers;
use Smorken\Sis\Contracts\Base\Klass\Material;
use Smorken\Sis\Db\Core\Klass\Fee;
use Smorken\Sis\Db\Incremental\Course\CourseNote;
use Smorken\Sis\Db\Incremental\Klass\ClassNote;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ClassModifiers), MapAttributeNames(new ClassAttributeMap)]
class Klass extends \Smorken\Sis\Db\Incremental\Klass\Klass implements \Smorken\Lookup\Contracts\Models\Klass
{
    protected array $relationMap = [
        Relations::MATERIALS => Material::class,
        Relations::ENROLLMENTS => Enrollment::class,
        Relations::MEETINGS => Meeting::class,
        Relations::CLASS_INSTRUCTORS => KlassInstructor::class,
        Relations::TERM => Term::class,
        Relations::CLASS_NOTES => ClassNote::class,
        Relations::COURSE_NOTES => CourseNote::class,
        Relations::FEES => Fee::class,
        Relations::OTHER => \Smorken\Sis\Db\Core\Klass\Klass::class,
        Relations::COMBINED_SECTIONS => \Smorken\Sis\Db\Core\Klass\Klass::class,
    ];

    public function asModel(): Model
    {
        return $this;
    }

    public function getLongName(): string
    {
        return sprintf('%s %s [%s]', $this->getShortName(), $this->classNumber, $this->termId);
    }

    public function getModelId(): string
    {
        return sprintf('%s%s', $this->termId, $this->classNumber);
    }

    public function getShortName(): string
    {
        return $this->className;
    }

    public function scopeOrderClassNameNumber(Builder $query): Builder
    {
        return $query->orderBy('CLASS_CLASS_NAME')
            ->orderBy('CLASS_CLASS_NBR');
    }

    public function scopeSubjectCatNumberLike(Builder $query, string $search): Builder
    {
        $search = $search.'%';

        return $query->where('CLASS_CLASS_NAME', 'LIKE', $search);
    }
}
