<?php

namespace Smorken\Lookup\Models\Eloquent;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\MeetingAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\MeetingModifiers;
use Smorken\Sis\Db\Core\Physical\Facility;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new MeetingModifiers), MapAttributeNames(new MeetingAttributeMap)]
class Meeting extends \Smorken\Sis\Db\Incremental\Klass\Meeting implements \Smorken\Lookup\Contracts\Models\Meeting
{
    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::CLASS_INSTRUCTORS => KlassInstructor::class,
        Relations::FACILITY => Facility::class,
    ];

    public function asModel(): Model
    {
        return $this;
    }

    public function getLongName(): string
    {
        return $this->getShortName();
    }

    public function getModelId(): string|int
    {
        return $this->sequenceNumber;
    }

    public function getShortName(): string
    {
        return sprintf('%s.%s', $this->sequenceNumber, $this->facilityId);
    }
}
