<?php

namespace Smorken\Lookup\Models\Eloquent;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\ClassInstructorAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\ClassInstructorModifiers;
use Smorken\Sis\Db\Core\Instructor\Position;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ClassInstructorModifiers), MapAttributeNames(new ClassInstructorAttributeMap)]
class KlassInstructor extends ClassInstructor implements \Smorken\Lookup\Contracts\Models\KlassInstructor
{
    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::MEETINGS => Meeting::class,
        Relations::INSTRUCTOR => Person::class,
        Relations::INSTRUCTOR_POSITIONS => Position::class,
    ];

    public function asModel(): Model
    {
        return $this;
    }

    public function getLongName(): string
    {
        return $this->getShortName();
    }

    public function getModelId(): string
    {
        return sprintf('%s%s', $this->sequenceNumber, $this->instructorId);
    }

    public function getShortName(): string
    {
        return sprintf('%s.%s', $this->sequenceNumber, $this->instructorId);
    }
}
