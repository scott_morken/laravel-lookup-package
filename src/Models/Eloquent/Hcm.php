<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:16 AM
 */

namespace Smorken\Lookup\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Hrms\Attributes\AttributeNames\Camel\HcmAttributeMap;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Contracts\Model;

#[MapAttributeNames(new HcmAttributeMap)]
class Hcm extends \Smorken\Hrms\Models\Eloquent\Hcm implements \Smorken\Lookup\Contracts\Models\Hcm
{
    /**
     * @return \Smorken\Lookup\Contracts\Models\Person
     */
    public function asModel(): Model
    {
        $attrs = [
            'PERS_EMPLID' => $this->getModelId(),
            'PERS_OPRID' => $this->alt_id,
            'PERS_PRIMARY_FIRST_NAME' => $this->first_name,
            'PERS_PRIMARY_LAST_NAME' => $this->last_name,
            'PERS_EMAIL_ADDR' => $this->email,
            'PERS_HOME_PHONE_NBR' => $this->phone,
        ];
        $m = (new \Smorken\Lookup\Models\Eloquent\Person)->forceFill($attrs);
        $m->setRelation('hcm', $this);

        return $m;
    }

    public function fullName(): string
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function getIdAttribute(): string
    {
        return $this->attributes['CAMPUS_ID'];
    }

    public function getLongName(): string
    {
        return sprintf(
            '%s &middot; %s &middot; Emp:%s/Fac:%s',
            $this->getShortName(),
            $this->alt_id,
            'Y',
            $this->isFaculty() ? 'Y' : 'N'
        );
    }

    public function getModelId(): string
    {
        return $this->getIdAttribute();
    }

    public function getShortName(): string
    {
        return $this->fullName();
    }

    public function isEmployee(): bool
    {
        return true;
    }

    public function scopeAltIdIs(Builder $query, string $alt_id): Builder
    {
        return $query->where('MC_MEID', '=', $alt_id);
    }

    public function scopeFirstNameLike(Builder $query, string $search): Builder
    {
        $search = $search.'%';
        $query->where(
            function ($q) use ($search) {
                $q->where('FIRST_NAME', 'LIKE', $search)
                    ->orWhere('MC_PREF_NAME', 'LIKE', $search);
            }
        );

        return $query;
    }

    public function scopeIdIs(Builder $query, string|int|array $id): Builder
    {
        return $query->where('CAMPUS_ID', '=', $id);
    }

    public function scopeNameLike(Builder $query, ?string $last, ?string $first): Builder
    {
        if ($last) {
            // @phpstan-ignore method.notFound
            $query = $query->lastNameLike($last);
        }
        if ($first) {
            $query = $query->firstNameLike($first);
        }

        return $query;
    }

    public function scopeOrderLastFirst(Builder $query): Builder
    {
        return $query->orderBy('LAST_NAME')
            ->orderBy('FIRST_NAME');
    }
}
