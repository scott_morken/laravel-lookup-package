<?php

namespace Smorken\Lookup\Models\Eloquent;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\EnrollmentAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\EnrollmentModifiers;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new EnrollmentModifiers), MapAttributeNames(new EnrollmentAttributeMap)]
class Enrollment extends \Smorken\Sis\Db\Incremental\Student\Enrollment implements \Smorken\Lookup\Contracts\Models\Enrollment
{
    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::STUDENT => Person::class,
        Relations::TERM => Term::class,
    ];

    public function asModel(): Model
    {
        return $this;
    }

    public function getLongName(): string
    {
        return $this->getShortName();
    }

    public function getModelId(): string
    {
        return sprintf('%s%s%s', $this->termId, $this->classNumber, $this->studentId);
    }

    public function getShortName(): string
    {
        return sprintf('%s.%s.%s', $this->termId, $this->classNumber, $this->studentId);
    }
}
