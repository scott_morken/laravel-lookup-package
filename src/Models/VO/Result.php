<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:28 AM
 */

namespace Smorken\Lookup\Models\VO;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\Models\Lookup;

class Result implements \Smorken\Lookup\Contracts\Models\Result
{
    public function __construct(
        public LookupType $type,
        public Collection|Paginator|Lookup|null $result
    ) {}

    public function hasResult(): bool
    {
        return $this->isCollection() || $this->isModel();
    }

    public function isCollection(): bool
    {
        return $this->result instanceof Collection || $this->result instanceof Paginator;
    }

    public function isModel(): bool
    {
        return $this->result instanceof Lookup;
    }
}
