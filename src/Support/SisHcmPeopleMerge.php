<?php

declare(strict_types=1);

namespace Smorken\Lookup\Support;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Lookup\Contracts\Models\Person;

class SisHcmPeopleMerge
{
    public function __construct(protected Person $person) {}

    public function merge(
        Collection|Paginator|null $sisPeople,
        Collection|Paginator|null $hcmPeople
    ): Collection|Paginator {
        $personModel = $this->person;
        /** @var Collection $sisPeople */
        $sisPeople = $this->ensureCollection($sisPeople);
        /** @var Collection $hcmPeople */
        $hcmPeople = $this->ensureCollection($hcmPeople);
        // @phpstan-ignore method.notFound
        $sisPeople->each(fn (Person $person) => $person->setRelation('hcm', null));
        if (count($hcmPeople) > 0) {
            $sisPeople = $sisPeople->keyBy(fn (Person $person) => (string) $person->getModelId());
            /** @var \Smorken\Lookup\Contracts\Models\Hcm $hcmPerson */
            foreach ($hcmPeople as $hcmPerson) {
                $sisPerson = $sisPeople->get((string) $hcmPerson->getModelId());
                if (! $sisPerson) {
                    $sisPeople->put((string) $hcmPerson->getModelId(), $personModel::fromHcmModel($hcmPerson));
                } else {
                    $sisPerson->setRelation('hcm', $hcmPerson);
                }
            }
            // @phpstan-ignore property.notFound, property.notFound
            $ordered = $sisPeople->sortBy(fn (Person $person) => $person->lastName.$person->firstName);

            return $ordered->slice(0, 50);
        }

        return $sisPeople;
    }

    protected function ensureCollection(Collection|Paginator|null $iterable): Collection|Paginator
    {
        if (is_null($iterable)) {
            return new Collection;
        }

        return $iterable;
    }
}
