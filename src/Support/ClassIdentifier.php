<?php

declare(strict_types=1);

namespace Smorken\Lookup\Support;

class ClassIdentifier
{
    public string $classNumber;

    public string $termId;

    public function __construct(string $identifier)
    {
        $separator = str_contains($identifier, ':') ? ':' : null;
        [$termId, $classNumber] = $this->getParts($identifier, $separator);
        $this->termId = $termId;
        $this->classNumber = $classNumber;
    }

    protected function getParts(string $identifier, ?string $separator): array
    {
        if ($separator) {
            return explode($separator, $identifier);
        }

        return [substr($identifier, 0, 4), substr($identifier, 4)];
    }
}
