<?php

declare(strict_types=1);

namespace Smorken\Lookup\Support;

class PersonIdentifier
{
    public function __construct(protected string|int $identifier) {}

    public function isAltId(): bool
    {
        if (is_numeric($this->identifier)) {
            return false;
        }

        return (bool) preg_match('([A-z]{3}[0-9]{7}|[A-z]{5}[0-9]{5})', $this->identifier);
    }

    public function isId(): bool
    {
        return filter_var($this->identifier, FILTER_VALIDATE_INT) !== false;
    }
}
