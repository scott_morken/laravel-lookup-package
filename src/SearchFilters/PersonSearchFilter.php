<?php

declare(strict_types=1);

namespace Smorken\Lookup\SearchFilters;

use Illuminate\Http\Request;

class PersonSearchFilter implements \Smorken\Lookup\Contracts\SearchFilters\PersonSearchFilter
{
    public function __construct(
        public string|int|null $id,
        public ?string $altId,
        public ?string $lastName,
        public ?string $firstName,
        public ?string $birthdate
    ) {}

    public static function fromRequest(Request $request): static
    {
        return new static(
            id: $request->input('filter.person.id'),
            altId: $request->input('filter.person.altId'),
            lastName: $request->input('filter.person.lastName'),
            firstName: $request->input('filter.person.firstName'),
            birthdate: $request->input('filter.person.birthdate')
        );
    }

    public function is(): bool
    {
        return $this->id || $this->altId || $this->lastName;
    }
}
