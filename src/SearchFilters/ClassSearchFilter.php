<?php

declare(strict_types=1);

namespace Smorken\Lookup\SearchFilters;

use Illuminate\Http\Request;

class ClassSearchFilter implements \Smorken\Lookup\Contracts\SearchFilters\ClassSearchFilter
{
    public function __construct(
        public ?string $id,
        public ?string $termId,
        public ?string $className,
        public ?string $classNumber
    ) {}

    public static function fromRequest(Request $request): static
    {
        return new static(
            id: $request->input('filter.class.id'),
            termId: $request->input('filter.class.termId'),
            className: $request->input('filter.class.className'),
            classNumber: $request->input('filter.class.classNumber'),
        );
    }

    public function is(): bool
    {
        return $this->id || $this->wantsClassNameSearch() || $this->wantsClassNumberSearch();
    }

    protected function wantsClassNameSearch(): bool
    {
        return $this->termId && $this->className;
    }

    protected function wantsClassNumberSearch(): bool
    {
        return $this->termId && $this->classNumber;
    }
}
