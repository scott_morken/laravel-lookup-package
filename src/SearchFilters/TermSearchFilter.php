<?php

declare(strict_types=1);

namespace Smorken\Lookup\SearchFilters;

use Carbon\Carbon;
use Illuminate\Http\Request;

class TermSearchFilter implements \Smorken\Lookup\Contracts\SearchFilters\TermSearchFilter
{
    public function __construct(
        public ?string $id,
        public string|Carbon|null $startDate,
        public string|Carbon|null $endDate
    ) {}

    public static function fromRequest(Request $request): static
    {
        return new static(
            id: $request->input('filter.term.id'),
            startDate: $request->input('filter.term.startDate'),
            endDate: $request->input('filter.term.endDate')
        );
    }

    public function is(): bool
    {
        return $this->id || $this->startDate || $this->endDate;
    }
}
