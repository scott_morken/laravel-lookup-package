<?php

declare(strict_types=1);

namespace Smorken\Lookup\Factories;

use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Lookup\Contracts\Repositories\PersonSisFindByAltIdRepository;
use Smorken\Lookup\Contracts\Repositories\PersonSisFindByIdRepository;
use Smorken\Lookup\Contracts\Repositories\PersonsSisGetByBirthdateRepository;
use Smorken\Lookup\Contracts\Repositories\PersonsSisGetByFirstLastNameRepository;

class PersonSisRepositoryFactory extends PersonModelRepositoryFactory implements LookupFactory
{
    protected array $handlers = [
        'find' => PersonSisFindByIdRepository::class,
        'findByAltId' => PersonSisFindByAltIdRepository::class,
        'birthdate' => PersonsSisGetByBirthdateRepository::class,
        'firstLastName' => PersonsSisGetByFirstLastNameRepository::class,
    ];

    public function emptyModel(): Person
    {
        return $this->handlerForEmptyModel();
    }
}
