<?php

namespace Smorken\Lookup\Factories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Term;
use Smorken\Lookup\Contracts\Repositories\TermFindActiveOrNextRepository;
use Smorken\Lookup\Contracts\Repositories\TermFindByIdRepository;
use Smorken\Lookup\Contracts\Repositories\TermsActiveAndUpcomingRepository;
use Smorken\Lookup\Contracts\Repositories\TermsAllRepository;
use Smorken\Lookup\Contracts\Repositories\TermsBetweenDatesRepository;
use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;

class TermRepositoryFactory extends RepositoryFactory implements LookupFactory
{
    protected array $handlers = [
        'activeAndUpcoming' => TermsActiveAndUpcomingRepository::class,
        'find' => TermFindByIdRepository::class,
    ];

    public function activeAndUpcoming(): Collection
    {
        return $this->forIterable($this->getHandler('activeAndUpcoming'), 0);
    }

    public function all(): Collection
    {
        return $this->forIterable(TermsAllRepository::class, 0);
    }

    public function find(string $id, bool $throw = true): ?Term
    {
        return $this->handlerForFind($id, $throw);
    }

    public function findActiveOrNext(): Term
    {
        return $this->for(TermFindActiveOrNextRepository::class);
    }

    public function getByDates(Carbon|string|null $start, Carbon|string|null $end): Collection|Paginator
    {
        return $this->for(TermsBetweenDatesRepository::class, ['start' => $start, 'end' => $end, 'perPage' => 0]);
    }

    /**
     * @param  \Smorken\Lookup\Contracts\SearchFilters\TermSearchFilter  $filter
     */
    public function search(SearchFilter $filter): Collection|Term|null
    {
        if ($filter->id) {
            return $this->find($filter->id);
        }
        if ($filter->startDate || $filter->endDate) {
            return $this->getByDates($filter->startDate, $filter->endDate);
        }

        return null;
    }
}
