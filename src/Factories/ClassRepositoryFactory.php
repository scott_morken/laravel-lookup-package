<?php

declare(strict_types=1);

namespace Smorken\Lookup\Factories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Klass;
use Smorken\Lookup\Contracts\Repositories\ClassesByClassNameAndTermIdRepository;
use Smorken\Lookup\Contracts\Repositories\ClassFindByIdentifierRepository;
use Smorken\Lookup\Contracts\Repositories\ClassFindByTermIdAndClassNumberRepository;
use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;

class ClassRepositoryFactory extends RepositoryFactory implements LookupFactory
{
    public function find(string $id, bool $throw = true): ?Klass
    {
        return $this->findByIdentifier($id, $throw);
    }

    public function findByIdentifier(string $identifier, bool $throw = true): ?Klass
    {
        return $this->forRetrieve(ClassFindByIdentifierRepository::class, $identifier, $throw);
    }

    public function findByTermIdAndClassNumber(string $termId, string $classNumber, bool $throw = true): ?Klass
    {
        return $this->for(ClassFindByTermIdAndClassNumberRepository::class,
            ['termId' => $termId, 'classNumber' => $classNumber, 'throw' => $throw]);
    }

    public function getByTermIdAndClassName(string $termId, string $className): Paginator|Collection
    {
        return $this->for(ClassesByClassNameAndTermIdRepository::class,
            ['className' => $className, 'termId' => $termId]);
    }

    /**
     * @param  \Smorken\Lookup\Contracts\SearchFilters\ClassSearchFilter  $filter
     */
    public function search(SearchFilter $filter): Paginator|Collection|Klass|null
    {
        if ($filter->id) {
            return $this->findByIdentifier($filter->id);
        }
        if ($filter->termId && $filter->classNumber) {
            return $this->findByTermIdAndClassNumber($filter->termId, $filter->classNumber);
        }
        if ($filter->termId && $filter->className) {
            return $this->getByTermIdAndClassName($filter->termId, $filter->className);
        }

        return null;
    }
}
