<?php

declare(strict_types=1);

namespace Smorken\Lookup\Factories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Hcm;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;

abstract class PersonModelRepositoryFactory extends RepositoryFactory implements LookupFactory
{
    public function find(string|int $id, bool $throw = true): Hcm|Person|null
    {
        return $this->handlerForFind($id, $throw);
    }

    public function findByAltId(string $id, bool $throw = true): Hcm|Person|null
    {
        return $this->forRetrieve($this->getHandler('findByAltId'), $id, $throw);
    }

    public function getByBirthdate(string|Carbon $birthdate, int $perPage = 50): Collection|Paginator
    {
        return $this->for($this->getHandler('birthdate'), ['birthdate' => $birthdate, 'perPage' => $perPage]);
    }

    public function getByFirstLastName(?string $firstName, ?string $lastName, int $perPage = 50): Collection|Paginator
    {
        return $this->for($this->getHandler('firstLastName'),
            ['firstName' => $firstName, 'lastName' => $lastName, 'perPage' => $perPage]);
    }

    /**
     * @param  \Smorken\Lookup\Contracts\SearchFilters\PersonSearchFilter  $filter
     */
    public function search(SearchFilter $filter): Collection|Paginator|Hcm|Person|null
    {
        if ($filter->id) {
            return $this->find($filter->id);
        }
        if ($filter->altId) {
            return $this->findByAltId($filter->altId);
        }
        if ($filter->lastName || $filter->firstName) {
            return $this->getByFirstLastName($filter->firstName, $filter->lastName);
        }
        if ($filter->birthdate) {
            return $this->getByBirthdate($filter->birthdate);
        }

        return null;
    }
}
