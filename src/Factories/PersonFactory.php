<?php

declare(strict_types=1);

namespace Smorken\Lookup\Factories;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Lookup\Constants\PersonType;
use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;
use Smorken\Lookup\Support\PersonIdentifier;
use Smorken\Lookup\Support\SisHcmPeopleMerge;

class PersonFactory implements LookupFactory
{
    public function __construct(
        protected array $factories
    ) {}

    public function find(string|int $id): ?Person
    {
        if (! $id) {
            return null;
        }
        $identifierHelper = new PersonIdentifier($id);
        if ($identifierHelper->isAltId()) {
            return $this->findByAltId($id);
        }

        return $this->findById($id);
    }

    /**
     * @param  \Smorken\Lookup\Contracts\SearchFilters\PersonSearchFilter  $filter
     */
    public function search(SearchFilter $filter): Paginator|Collection|Person|null
    {
        if ($filter->id) {
            return $this->find($filter->id);
        }
        if ($filter->altId) {
            return $this->findByAltId($filter->altId);
        }
        if ($filter->lastName || $filter->firstName) {
            return $this->getByFirstLastName($filter->firstName, $filter->lastName);
        }
        if ($filter->birthdate) {
            return $this->getByBirthdate($filter->birthdate);
        }

        return null;
    }

    protected function findByAltId(string $id): ?Person
    {
        // @phpstan-ignore method.notFound
        if ($sis = $this->getFactory(PersonType::SIS)->findByAltId($id, false)) {
            return $sis;
        }
        // @phpstan-ignore method.notFound
        $hcm = $this->getFactory(PersonType::HCM)->findByAltId($id, false);
        if ($hcm) {
            /** @var \Smorken\Lookup\Contracts\Models\Person $personModel */
            $personModel = $this->getFactory(PersonType::SIS)->emptyModel();

            return $personModel::fromHcmModel($hcm);
        }

        return null;
    }

    protected function findById(string|int $id): ?Person
    {
        if ($sis = $this->getFactory(PersonType::SIS)->find($id, false)) {
            return $sis;
        }
        $hcm = $this->getFactory(PersonType::HCM)->find($id, false);
        if ($hcm) {
            /** @var \Smorken\Lookup\Contracts\Models\Person $personModel */
            $personModel = $this->getFactory(PersonType::SIS)->emptyModel();

            return $personModel::fromHcmModel($hcm);
        }

        return null;
    }

    protected function getByFirstLastName(?string $firstName, ?string $lastName): Collection|Paginator
    {
        // @phpstan-ignore method.notFound
        $sisPeople = $this->getFactory(PersonType::SIS)->getByFirstLastName($firstName, $lastName);
        // @phpstan-ignore method.notFound
        $hcmPeople = $this->getFactory(PersonType::HCM)->getByFirstLastName($firstName, $lastName);
        $merger = new SisHcmPeopleMerge($this->getFactory(PersonType::SIS)->emptyModel());

        return $merger->merge($sisPeople, $hcmPeople);
    }

    protected function getByBirthdate(string|Carbon $birthdate): Collection|Paginator
    {
        // @phpstan-ignore method.notFound
        $sisPeople = $this->getFactory(PersonType::SIS)->getByBirthdate($birthdate);
        // @phpstan-ignore method.notFound
        $hcmPeople = $this->getFactory(PersonType::HCM)->getByBirthdate($birthdate);
        $merger = new SisHcmPeopleMerge($this->getFactory(PersonType::SIS)->emptyModel());

        return $merger->merge($sisPeople, $hcmPeople);
    }

    protected function getFactory(PersonType|string $type): RepositoryFactory
    {
        if (is_string($type)) {
            $type = PersonType::from($type);
        }

        return $this->factories[$type->value] ?? throw new \OutOfBoundsException("[$type->value] factory not found.");
    }
}
