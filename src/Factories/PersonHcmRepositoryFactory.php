<?php

declare(strict_types=1);

namespace Smorken\Lookup\Factories;

use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Repositories\PersonHcmFindByAltIdRepository;
use Smorken\Lookup\Contracts\Repositories\PersonHcmFindByIdRepository;
use Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByBirthdateRepository;
use Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByFirstLastNameRepository;

class PersonHcmRepositoryFactory extends PersonModelRepositoryFactory implements LookupFactory
{
    protected array $handlers = [
        'find' => PersonHcmFindByIdRepository::class,
        'findByAltId' => PersonHcmFindByAltIdRepository::class,
        'birthdate' => PersonsHcmGetByBirthdateRepository::class,
        'firstLastName' => PersonsHcmGetByFirstLastNameRepository::class,
    ];
}
