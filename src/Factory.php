<?php

declare(strict_types=1);

namespace Smorken\Lookup;

use Illuminate\Http\Request;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\Factories\LookupFactory;
use Smorken\Lookup\Contracts\Models\Result as ResultContract;
use Smorken\Lookup\Contracts\SearchFilters\ClassSearchFilter;
use Smorken\Lookup\Contracts\SearchFilters\PersonSearchFilter;
use Smorken\Lookup\Contracts\SearchFilters\SearchFilter;
use Smorken\Lookup\Contracts\SearchFilters\TermSearchFilter;
use Smorken\Lookup\Models\VO\Result;

class Factory
{
    protected array $filters = [];

    public function __construct(protected array $repositoryFactories) {}

    public function create(LookupType $type): LookupFactory
    {
        return $this->repositoryFactories[$type->value];
    }

    public function fromRequest(Request $request): ResultContract|false
    {
        foreach ($this->getFilters($request) as $filter) {
            if ($filter->is() && ($r = $this->search($filter))) {
                return $r;
            }
        }

        return false;
    }

    public function search(SearchFilter $filter): ResultContract|false
    {
        if (is_a($filter, ClassSearchFilter::class)) {
            return new Result(LookupType::KLASS, $this->create(LookupType::KLASS)->search($filter));
        }
        if (is_a($filter, PersonSearchFilter::class)) {
            return new Result(LookupType::PERSON, $this->create(LookupType::PERSON)->search($filter));
        }
        if (is_a($filter, TermSearchFilter::class)) {
            return new Result(LookupType::TERM, $this->create(LookupType::TERM)->search($filter));
        }

        return false;
    }

    public function willSearch(Request $request): bool
    {
        foreach ($this->getFilters($request) as $filter) {
            if ($filter->is()) {
                return true;
            }
        }

        return false;
    }

    protected function getFilters(Request $request): array
    {
        if (empty($this->filters)) {
            $this->filters = [
                \Smorken\Lookup\SearchFilters\ClassSearchFilter::fromRequest($request),
                \Smorken\Lookup\SearchFilters\PersonSearchFilter::fromRequest($request),
                \Smorken\Lookup\SearchFilters\TermSearchFilter::fromRequest($request),
            ];
        }

        return $this->filters;
    }
}
