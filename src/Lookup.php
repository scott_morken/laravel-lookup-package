<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 7:40 AM
 */

namespace Smorken\Lookup;

use Illuminate\Support\Arr;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\Lookups\Base;
use Smorken\Lookup\Models\VO\Result;
use Smorken\Model\Contracts\Model;

/**
 * @deprecated
 */
class Lookup implements \Smorken\Lookup\Contracts\Lookup
{
    protected array $lookups = [
        'class' => null,
        'person' => null,
        'term' => null,
    ];

    public function __construct(array $lookups)
    {
        foreach ($lookups as $type => $lookup) {
            $this->setLookup($type, $lookup);
        }
    }

    public function byId(string $type, string $id): ?Model
    {
        return $this->getLookup($type)
            ->byId($id);
    }

    public function getLookup(string $type): Base
    {
        if (array_key_exists($type, $this->lookups) && $this->lookups[$type]) {
            return $this->lookups[$type];
        }
        throw new \InvalidArgumentException("$type is not a valid provider.");
    }

    public function lookup(array $filter): Contracts\Models\Result
    {
        if ($this->isPersonSearch($filter)) {
            return $this->getResult('person', $this->getPersonIdentifier($filter));
        }
        if ($this->isTermSearch($filter)) {
            return $this->getResult('term', $this->getTermIdentifier($filter));
        }
        if ($this->isKlassSearch($filter)) {
            return $this->getResult('class', $this->getKlassIdentifier($filter));
        }
        throw new \OutOfBoundsException('No filter provided to lookup.');
    }

    public function setLookup(string $type, Base $provider): void
    {
        $this->lookups[$type] = $provider;
    }

    public function shouldLookup(array $filter): bool
    {
        foreach ($this->lookups as $k => $l) {
            $method = sprintf('is%sSearch', ucfirst($k));
            if (method_exists($this, $method) && $this->$method($filter)) {
                return true;
            }
        }

        return false;
    }

    protected function getKlassIdentifier(array $filter): array
    {
        return [Arr::get($filter, 'klass.id'), Arr::get($filter, 'klass.term')];
    }

    protected function getPersonIdentifier(array $filter): string|array
    {
        if (Arr::get($filter, 'person.id')) {
            return Arr::get($filter, 'person.id');
        }

        return [Arr::get($filter, 'person.last'), Arr::get($filter, 'person.first')];
    }

    protected function getResult($type, $identifier): Contracts\Models\Result
    {
        return $this->getResultModel($type, $this->getLookup($type)
            ->find($identifier));
    }

    protected function getResultModel($type, $result): Contracts\Models\Result
    {
        if (is_string($type)) {
            $type = LookupType::from($type);
        }

        return new Result($type, $result);
    }

    protected function getTermIdentifier(array $filter): string
    {
        return Arr::get($filter, 'term.id');
    }

    protected function isKlassSearch(array $filter): bool
    {
        return Arr::get($filter, 'klass.id') && Arr::get($filter, 'klass.term');
    }

    protected function isPersonSearch(array $filter): bool
    {
        return Arr::get($filter, 'person.id') || Arr::get($filter, 'person.last');
    }

    protected function isTermSearch(array $filter): bool
    {
        return (bool) Arr::get($filter, 'term.id');
    }
}
