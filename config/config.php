<?php

return [
    'class' => [
        'impl' => \Smorken\Lookup\Lookups\WithProvider\Klass::class,
        'providers' => [
            'klass' => [
                'impl' => \Smorken\Lookup\Storage\Eloquent\Klass::class,
                'model' => \Smorken\Lookup\Models\Eloquent\Klass::class,
            ],
        ],
    ],
    'person' => [
        'impl' => \Smorken\Lookup\Lookups\WithProvider\Person::class,
        'providers' => [
            'hcm' => [
                'impl' => \Smorken\Lookup\Storage\Eloquent\Hcm::class,
                'model' => \Smorken\Lookup\Models\Eloquent\Hcm::class,
            ],
            'person' => [
                'impl' => \Smorken\Lookup\Storage\Eloquent\Person::class,
                'model' => \Smorken\Lookup\Models\Eloquent\Person::class,
            ],
        ],
    ],
    'term' => [
        'impl' => \Smorken\Lookup\Lookups\WithProvider\Term::class,
        'providers' => [
            'term' => [
                'impl' => \Smorken\Lookup\Storage\Eloquent\Term::class,
                'model' => \Smorken\Lookup\Models\Eloquent\Term::class,
            ],
        ],
    ],
    'models' => [
        \Smorken\Lookup\Contracts\Models\Hcm::class => \Smorken\Lookup\Models\Eloquent\Hcm::class,
        \Smorken\Lookup\Contracts\Models\Klass::class => \Smorken\Lookup\Models\Eloquent\Klass::class,
        \Smorken\Lookup\Contracts\Models\Person::class => \Smorken\Lookup\Models\Eloquent\Person::class,
        \Smorken\Lookup\Contracts\Models\Term::class => \Smorken\Lookup\Models\Eloquent\Term::class,
    ],
    'repositories' => [
        \Smorken\Lookup\Contracts\Repositories\ClassesByClassNameAndTermIdRepository::class => \Smorken\Lookup\Repositories\ClassesByClassNameAndTermIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\ClassFindByIdentifierRepository::class => \Smorken\Lookup\Repositories\ClassFindByIdentifierRepository::class,
        \Smorken\Lookup\Contracts\Repositories\ClassFindByTermIdAndClassNumberRepository::class => \Smorken\Lookup\Repositories\ClassFindByTermIdAndClassNumberRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonHcmFindByAltIdRepository::class => \Smorken\Lookup\Repositories\PersonHcmFindByAltIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonSisFindByAltIdRepository::class => \Smorken\Lookup\Repositories\PersonSisFindByAltIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonHcmFindByIdRepository::class => \Smorken\Lookup\Repositories\PersonHcmFindByIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonSisFindByIdRepository::class => \Smorken\Lookup\Repositories\PersonSisFindByIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByBirthdateRepository::class => \Smorken\Lookup\Repositories\PersonsHcmGetByBirthdateRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonsSisGetByBirthdateRepository::class => \Smorken\Lookup\Repositories\PersonsSisGetByBirthdateRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonsHcmGetByFirstLastNameRepository::class => \Smorken\Lookup\Repositories\PersonsHcmGetByFirstLastNameRepository::class,
        \Smorken\Lookup\Contracts\Repositories\PersonsSisGetByFirstLastNameRepository::class => \Smorken\Lookup\Repositories\PersonsSisGetByFirstLastNameRepository::class,
        \Smorken\Lookup\Contracts\Repositories\TermFindActiveOrNextRepository::class => \Smorken\Lookup\Repositories\TermFindActiveOrNextRepository::class,
        \Smorken\Lookup\Contracts\Repositories\TermFindByIdRepository::class => \Smorken\Lookup\Repositories\TermFindByIdRepository::class,
        \Smorken\Lookup\Contracts\Repositories\TermsAllRepository::class => \Smorken\Lookup\Repositories\TermsAllRepository::class,
        \Smorken\Lookup\Contracts\Repositories\TermsActiveAndUpcomingRepository::class => \Smorken\Lookup\Repositories\TermsActiveAndUpcomingRepository::class,
    ],
];
