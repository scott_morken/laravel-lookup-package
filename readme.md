## Lookup base library for Laravel 5.5+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Add the service provider to `config/app.php`

Use the lookup provider

```
$lookup = app(\Smorken\Lookup\Contracts\Lookup::class);

//person lookups
$collection = $lookup->find(['last' => 'Bub', 'first' => 'Bob']);
$person = $lookup->find('12345678');//id
$person = $lookup->find('ABC1234567');//alt id
$person = $lookup->byId('12345678');

//klass lookups
$collection = $lookup->find('CIS', '1234');//subject[catalog number] and term
$klass = $lookup->find('12345', '1234');//class number and term
$klass = $lookup->byId('123412345');//combined term and class number identifier

//term lookups
$term = $lookup->find('1234');//term id
$term = $lookup->byId('1234');//term id
```
