<?php

namespace Tests\Smorken\Lookup\Concerns;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\SqlServerConnection;
use Mockery as m;
use Tests\Smorken\Lookup\Stubs\MockPDO;

trait WithMockConnection
{
    protected MockPDO|m\MockInterface|null $pdo = null;

    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $pdo = m::mock(MockPDO::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();
        $this->pdo = $pdo;

        return $connection;
    }

    protected function initConnectionResolver(string $connectionName = 'db', ?string $connectionClass = null): void
    {
        $connectionClass = $connectionClass ?? MySqlConnection::class;
        $cr = new ConnectionResolver([$connectionName => $this->getMockConnection($connectionClass)]);
        $cr->setDefaultConnection($connectionName);
        Model::setConnectionResolver($cr);
    }

    protected function initSisConnectionResolver(): void
    {
        $this->initConnectionResolver('sis', SqlServerConnection::class);
    }
}
