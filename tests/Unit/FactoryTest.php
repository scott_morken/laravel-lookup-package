<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit;

use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\SearchFilters\ClassSearchFilter;
use Smorken\Lookup\Contracts\SearchFilters\PersonSearchFilter;
use Smorken\Lookup\Contracts\SearchFilters\TermSearchFilter;
use Smorken\Lookup\Factories\ClassRepositoryFactory;
use Smorken\Lookup\Factories\PersonFactory;
use Smorken\Lookup\Factories\TermRepositoryFactory;
use Smorken\Lookup\Factory;
use Smorken\Lookup\Models\Eloquent\Klass;
use Smorken\Lookup\Models\Eloquent\Person;
use Smorken\Lookup\Models\Eloquent\Term;

class FactoryTest extends TestCase
{
    #[Test]
    public function it_searches_for_a_class_via_request_params(): void
    {
        $repos = [
            LookupType::KLASS->value => m::mock(ClassRepositoryFactory::class),
            LookupType::PERSON->value => m::mock(PersonFactory::class),
            LookupType::TERM->value => m::mock(TermRepositoryFactory::class),
        ];
        $resultModel = new Klass;
        $repos[LookupType::KLASS->value]->shouldReceive('search')
            ->with(m::type(ClassSearchFilter::class))
            ->andReturnUsing(function (ClassSearchFilter $filter) use ($resultModel) {
                $this->assertEquals('123456', $filter->id);

                return $resultModel;
            });
        $sut = new Factory($repos);
        $request = (new Request)->merge(['filter' => ['class' => ['id' => '123456']]]);
        $result = $sut->fromRequest($request);
        $this->assertEquals($resultModel, $result->result);
        $this->assertEquals(LookupType::KLASS, $result->type);
    }

    #[Test]
    public function it_searches_for_a_person_via_request_params(): void
    {
        $repos = [
            LookupType::KLASS->value => m::mock(ClassRepositoryFactory::class),
            LookupType::PERSON->value => m::mock(PersonFactory::class),
            LookupType::TERM->value => m::mock(TermRepositoryFactory::class),
        ];
        $resultModel = new Person;
        $repos[LookupType::PERSON->value]->shouldReceive('search')
            ->with(m::type(PersonSearchFilter::class))
            ->andReturnUsing(function (PersonSearchFilter $filter) use ($resultModel) {
                $this->assertEquals('123456', $filter->id);

                return $resultModel;
            });
        $sut = new Factory($repos);
        $request = (new Request)->merge(['filter' => ['person' => ['id' => '123456']]]);
        $result = $sut->fromRequest($request);
        $this->assertEquals($resultModel, $result->result);
        $this->assertEquals(LookupType::PERSON, $result->type);
    }

    #[Test]
    public function it_searches_for_a_term_via_request_params(): void
    {
        $repos = [
            LookupType::KLASS->value => m::mock(ClassRepositoryFactory::class),
            LookupType::PERSON->value => m::mock(PersonFactory::class),
            LookupType::TERM->value => m::mock(TermRepositoryFactory::class),
        ];
        $resultModel = new Term;
        $repos[LookupType::TERM->value]->shouldReceive('search')
            ->with(m::type(TermSearchFilter::class))
            ->andReturnUsing(function (TermSearchFilter $filter) use ($resultModel) {
                $this->assertEquals('123456', $filter->id);

                return $resultModel;
            });
        $sut = new Factory($repos);
        $request = (new Request)->merge(['filter' => ['term' => ['id' => '123456']]]);
        $result = $sut->fromRequest($request);
        $this->assertEquals($resultModel, $result->result);
        $this->assertEquals(LookupType::TERM, $result->type);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
