<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 9:53 AM
 */

namespace Tests\Smorken\Lookup\Unit\Lookups;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Lookups\WithProvider\Term;

class TermTest extends TestCase
{
    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testById(): void
    {
        $sut = $this->getSut();
        $term = new \Smorken\Lookup\Models\Eloquent\Term;
        $sut->getProvider('term')
            ->shouldReceive('findById')
            ->once()
            ->with('1234')
            ->andReturn($term);
        $this->assertSame($term, $sut->byId('1234'));
    }

    public function testFind(): void
    {
        $sut = $this->getSut();
        $term = new \Smorken\Lookup\Models\Eloquent\Term;
        $sut->getProvider('term')
            ->shouldReceive('findById')
            ->once()
            ->with('1234')
            ->andReturn($term);
        $this->assertSame($term, $sut->find('1234'));
    }

    protected function getSut(): \Smorken\Lookup\Contracts\Lookups\Term
    {
        $p = m::mock(\Smorken\Lookup\Contracts\Storage\Term::class);

        return new Term(['term' => $p]);
    }
}
