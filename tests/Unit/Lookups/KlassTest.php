<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 9:38 AM
 */

namespace Tests\Smorken\Lookup\Unit\Lookups;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Lookups\WithProvider\Klass;

class KlassTest extends TestCase
{
    public function testByIdValidKlassNumber(): void
    {
        $sut = $this->getSut();
        $klass = new \Smorken\Lookup\Models\Eloquent\Klass;
        $sut->getProvider('klass')
            ->shouldReceive('findById')
            ->once()
            ->with('123412345')
            ->andReturn($klass);
        $this->assertSame($klass, $sut->byId(123412345));
    }

    public function testFindDefaultsToSubjectCatNumber(): void
    {
        $sut = $this->getSut();
        $klasses = new Collection([
            new \Smorken\Lookup\Models\Eloquent\Klass,
        ]);
        $sut->getProvider('klass')
            ->shouldReceive('findByKlassNumberAndTerm')
            ->never();
        $sut->getProvider('klass')
            ->shouldReceive('findBySubjectCatNumberAndTerm')
            ->once()
            ->with('FOO', '1234')
            ->andReturn($klasses);
        $this->assertSame($klasses, $sut->find(['FOO', '1234']));
    }

    public function testFindWithKlassNumber(): void
    {
        $sut = $this->getSut();
        $klass = new \Smorken\Lookup\Models\Eloquent\Klass;
        $sut->getProvider('klass')
            ->shouldReceive('findByKlassNumberAndTerm')
            ->once()
            ->with('12345', '1234')
            ->andReturn($klass);
        $sut->getProvider('klass')
            ->shouldReceive('findBySubjectCatNumberAndTerm')
            ->never();
        $this->assertSame($klass, $sut->find(['12345', '1234']));
    }

    public function testFindWithKlassNumberInvalidNumberFallsThrough(): void
    {
        $sut = $this->getSut();
        $klasses = new Collection;
        $sut->getProvider('klass')
            ->shouldReceive('findByKlassNumberAndTerm')
            ->never();
        $sut->getProvider('klass')
            ->shouldReceive('findBySubjectCatNumberAndTerm')
            ->once()
            ->with('1234567', '1234')
            ->andReturn($klasses);
        $this->assertSame($klasses, $sut->find(['1234567', '1234']));
    }

    protected function getSut(): \Smorken\Lookup\Contracts\Lookups\Klass
    {
        $p = m::mock(\Smorken\Lookup\Contracts\Storage\Klass::class);

        return new Klass(['klass' => $p]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
