<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 9:42 AM
 */

namespace Tests\Smorken\Lookup\Unit\Lookups;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Contracts\Storage\Hcm;
use Smorken\Lookup\Lookups\WithProvider\Person;

class PersonTest extends TestCase
{
    public function testByIdFromHcmStorage(): void
    {
        $sut = $this->getSut();
        $person = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill(['CAMPUS_ID' => '123456']);
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->once()
            ->with('123456')
            ->andReturnNull();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->once()
            ->with('123456')
            ->andReturn($person);
        $this->assertEquals('123456', $sut->byId('123456')->id);
    }

    public function testByIdFromPersonStorage(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->once()
            ->with('123456')
            ->andReturn($person);
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $this->assertSame($person, $sut->byId('123456'));
    }

    public function testByIdNotIntIsNull(): void
    {
        $sut = $this->getSut();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $this->assertNull($sut->byId('123abc456'));
    }

    public function testFindNoPersonMatchHcmFirstLast(): void
    {
        $sut = $this->getSut();
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturnNull();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $p = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill([
            'CAMPUS_ID' => '123456', 'last_name' => 'Last', 'first_name' => 'First',
        ]);
        $c = new Collection([$p]);
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturn($c);
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $r = $sut->find(['first', 'last'])->first();
        $this->assertInstanceOf(\Smorken\Lookup\Contracts\Models\Person::class, $r);
        $this->assertEquals('Last', $r->last_name);
        $this->assertEquals('123456', $r->id);
    }

    public function testFindNoPersonMatchHcmMeid(): void
    {
        $sut = $this->getSut();
        $person = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill(['CAMPUS_ID' => '123456']);
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->once()
            ->with('ABC1234567')
            ->andReturnNull();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->once()
            ->with('ABC1234567')
            ->andReturn($person);
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $this->assertEquals('123456', $sut->find('ABC1234567')->id);
    }

    public function testFindNoPersonMatchHcmSid(): void
    {
        $sut = $this->getSut();
        $person = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill(['CAMPUS_ID' => '123456']);
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->once()
            ->with('123456')
            ->andReturnNull();
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->once()
            ->with('123456')
            ->andReturn($person);
        $m = $sut->find('123456');
        $this->assertEquals('123456', $m->id);
    }

    public function testFindNoPersonMatchNoHcmMatchIsNull(): void
    {
        $sut = $this->getSut();
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $this->assertNull($sut->find('foo bar'));
    }

    public function testPersonAndHcmCanMergeCollections(): void
    {
        \Smorken\Lookup\Models\Eloquent\Person::unguard(true);
        $p1 = new \Smorken\Lookup\Models\Eloquent\Person(
            ['PERS_EMPLID' => '1234', 'PERS_PRIMARY_FIRST_NAME' => 'Bob', 'PERS_PRIMARY_LAST_NAME' => 'Bobbert']
        );
        $sut = $this->getSut();
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('last', 'first')
            ->andReturn(new Collection([$p1]));
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        \Smorken\Lookup\Models\Eloquent\Hcm::unguard(true);
        $h1 = new \Smorken\Lookup\Models\Eloquent\Hcm(
            ['CAMPUS_ID' => '1234', 'LAST_NAME' => 'Bobbert', 'FIRST_NAME' => 'Bob']
        );
        $h2 = new \Smorken\Lookup\Models\Eloquent\Hcm(
            ['CAMPUS_ID' => '2345', 'LAST_NAME' => 'Adamson', 'FIRST_NAME' => 'Adam']
        );
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('last', 'first')
            ->andReturn(new Collection([$h1, $h2]));
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $r = $sut->find(['last', 'first']);
        $this->assertCount(2, $r);
        $this->assertEquals('2345', $r->first()->getModelId());
        $this->assertEquals('1234', $r->last()->getModelId());
    }

    public function testPersonAndHcmMergeKeepsPerson(): void
    {
        \Smorken\Lookup\Models\Eloquent\Person::unguard(true);
        $p1 = new \Smorken\Lookup\Models\Eloquent\Person(
            ['PERS_EMPLID' => '1234', 'PERS_PRIMARY_FIRST_NAME' => 'Bob', 'PERS_PRIMARY_LAST_NAME' => 'Bobbert']
        );
        $sut = $this->getSut();
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('last', 'first')
            ->andReturn(new Collection([$p1]));
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        \Smorken\Lookup\Models\Eloquent\Hcm::unguard(true);
        $h1 = new \Smorken\Lookup\Models\Eloquent\Hcm(
            ['CAMPUS_ID' => '1234', 'LAST_NAME' => 'Bobbert', 'FIRST_NAME' => 'Bob']
        );
        $h2 = new \Smorken\Lookup\Models\Eloquent\Hcm(
            ['CAMPUS_ID' => '2345', 'LAST_NAME' => 'Adamson', 'FIRST_NAME' => 'Adam']
        );
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('last', 'first')
            ->andReturn(new Collection([$h1, $h2]));
        $sut->getProvider('hcm')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findById')
            ->never();
        $r = $sut->find(['last', 'first']);
        $this->assertInstanceOf(\Smorken\Lookup\Contracts\Models\Person::class, $r->last());
    }

    public function testfindByAltIdFiveFive(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->once()
            ->with('abcDe12345')
            ->andReturn($person);
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $this->assertSame($person, $sut->find('abcDe12345'));
    }

    public function testfindByAltIdThreeSeven(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->once()
            ->with('abc1234567')
            ->andReturn($person);
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $this->assertSame($person, $sut->find('abc1234567'));
    }

    public function testfindById(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->once()
            ->with('30000000')
            ->andReturn($person);
        $this->assertSame($person, $sut->find('30000000'));
    }

    public function testfindByLastFirstMixesHcmDifferentPerson(): void
    {
        $sut = $this->getSut();
        $person = (new \Smorken\Lookup\Models\Eloquent\Person)->forceFill([
            'PERS_EMPLID' => '123456', 'PERS_PRIMARY_FIRST_NAME' => 'first', 'PERS_PRIMARY_LAST_NAME' => 'last',
        ]);
        $hcmPerson = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill([
            'CAMPUS_ID' => '55555', 'LAST_NAME' => 'last', 'FIRST_NAME' => 'first',
        ]);
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturn(new Collection([$person]));
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturn(new Collection([$hcmPerson]));
        $results = $sut->find(['first', 'last']);
        $this->assertCount(2, $results);
        $this->assertArrayHasKey('k123456', $results);
        $this->assertArrayHasKey('k55555', $results);
    }

    public function testfindByLastFirstMixesHcmSamePerson(): void
    {
        $sut = $this->getSut();
        $person = (new \Smorken\Lookup\Models\Eloquent\Person)->forceFill([
            'PERS_EMPLID' => '123456', 'PERS_PRIMARY_FIRST_NAME' => 'first', 'PERS_PRIMARY_LAST_NAME' => 'last',
        ]);
        $hcmPerson = (new \Smorken\Lookup\Models\Eloquent\Hcm)->forceFill([
            'CAMPUS_ID' => '123456', 'LAST_NAME' => 'last', 'FIRST_NAME' => 'first',
        ]);
        $sut->getProvider('person')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturn(new Collection([$person]));
        $sut->getProvider('person')
            ->shouldReceive('findByAltId')
            ->never();
        $sut->getProvider('person')
            ->shouldReceive('findById')
            ->never();
        $sut->getProvider('hcm')
            ->shouldReceive('findByLastFirst')
            ->once()
            ->with('first', 'last')
            ->andReturn(new Collection([$hcmPerson]));
        $results = $sut->find(['first', 'last']);
        $this->assertCount(1, $results);
        $this->assertArrayHasKey('k123456', $results);
    }

    protected function getSut(): \Smorken\Lookup\Contracts\Lookups\Person
    {
        $p = m::mock(\Smorken\Lookup\Contracts\Storage\Person::class);
        $ap = m::mock(Hcm::class);

        return new Person(['hcm' => $ap, 'person' => $p]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
