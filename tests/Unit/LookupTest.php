<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/17/18
 * Time: 9:26 AM
 */

namespace Tests\Smorken\Lookup\Unit;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\Lookups\Klass;
use Smorken\Lookup\Contracts\Lookups\Person;
use Smorken\Lookup\Contracts\Lookups\Term;
use Smorken\Lookup\Lookup;

class LookupTest extends TestCase
{
    public function testById(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $sut->getLookup('person')
            ->shouldReceive('byId')
            ->once()
            ->with('foo')
            ->andReturn($person);
        $this->assertSame($person, $sut->byId('person', 'foo'));
    }

    public function testInvalidProviderIsException(): void
    {
        $sut = $this->getSut();
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('foo is not a valid provider');
        $sut->getLookup('foo');
    }

    public function testLookupWithKlass(): void
    {
        $sut = $this->getSut();
        $klasses = new Collection([
            new \Smorken\Lookup\Models\Eloquent\Klass,
        ]);
        $filter = ['klass' => ['id' => 'foo', 'term' => '1234']];
        $sut->getLookup('class')
            ->shouldReceive('find')
            ->with(['foo', '1234'])
            ->once()
            ->andReturn($klasses);
        $result = $sut->lookup($filter);
        $this->assertEquals(LookupType::KLASS, $result->type);
        $this->assertSame($klasses, $result->result);
    }

    public function testLookupWithPersonId(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $filter = ['person' => ['id' => 'foo']];
        $sut->getLookup('person')
            ->shouldReceive('find')
            ->with('foo')
            ->once()
            ->andReturn($person);
        $result = $sut->lookup($filter);
        $this->assertEquals(LookupType::PERSON, $result->type);
        $this->assertSame($person, $result->result);
    }

    public function testLookupWithPersonIdOverridesFirstLast(): void
    {
        $sut = $this->getSut();
        $person = new \Smorken\Lookup\Models\Eloquent\Person;
        $filter = ['person' => ['id' => 'someid', 'first' => 'foo', 'last' => 'bar']];
        $sut->getLookup('person')
            ->shouldReceive('find')
            ->with('someid')
            ->once()
            ->andReturn($person);
        $result = $sut->lookup($filter);
        $this->assertEquals(LookupType::PERSON, $result->type);
        $this->assertSame($person, $result->result);
    }

    public function testLookupWithPersonLastFirst(): void
    {
        $sut = $this->getSut();
        $filter = ['person' => ['first' => 'foo', 'last' => 'bar']];
        $people = new Collection([
            new \Smorken\Lookup\Models\Eloquent\Person,
        ]);
        $sut->getLookup('person')
            ->shouldReceive('find')
            ->with(['bar', 'foo'])
            ->once()
            ->andReturn($people);
        $result = $sut->lookup($filter);
        $this->assertEquals(LookupType::PERSON, $result->type);
        $this->assertSame($people, $result->result);
    }

    public function testLookupWithTerm(): void
    {
        $sut = $this->getSut();
        $term = new \Smorken\Lookup\Models\Eloquent\Term;
        $filter = ['term' => ['id' => '1234']];
        $sut->getLookup('term')
            ->shouldReceive('find')
            ->with('1234')
            ->once()
            ->andReturn($term);
        $result = $sut->lookup($filter);
        $this->assertEquals(LookupType::TERM, $result->type);
        $this->assertSame($term, $result->result);
    }

    public function testLookupWithoutValidFilterIsException(): void
    {
        $sut = $this->getSut();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('No filter provided to lookup');
        $sut->lookup([]);
    }

    protected function getSut(): \Smorken\Lookup\Contracts\Lookup
    {
        $p = [
            'class' => m::mock(Klass::class),
            'person' => m::mock(Person::class),
            'term' => m::mock(Term::class),
        ];

        return new Lookup($p);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
