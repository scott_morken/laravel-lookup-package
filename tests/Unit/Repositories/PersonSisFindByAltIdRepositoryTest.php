<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Person;
use Smorken\Lookup\Repositories\PersonSisFindByAltIdRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonSisFindByAltIdRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_finds_using_alt_id(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [CDS_PERSON_TBL] where [PERS_OPRID] = ? order by [PERS_PRIMARY_LAST_NAME] asc, [PERS_PRIMARY_FIRST_NAME] asc')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, 'abc1234567', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['PERS_OPRID' => 'abc1234567']]);
        $sut = new PersonSisFindByAltIdRepository(new Person);
        $person = $sut('abc1234567');
        $this->assertEquals('abc1234567', $person->altId);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
