<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Hcm;
use Smorken\Lookup\Repositories\PersonHcmFindByAltIdRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonHcmFindByAltIdRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_finds_using_alt_id(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [PS_MC_HCM] where [MC_MEID] = ? order by [LAST_NAME] asc, [FIRST_NAME] asc')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, 'abc1234567', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['MC_MEID' => 'abc1234567']]);
        $sut = new PersonHcmFindByAltIdRepository(new Hcm);
        $person = $sut('abc1234567');
        $this->assertEquals('abc1234567', $person->altId);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
