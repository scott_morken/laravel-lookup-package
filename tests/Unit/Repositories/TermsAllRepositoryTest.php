<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Term;
use Smorken\Lookup\Repositories\TermsAllRepository;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class TermsAllRepositoryTest extends TestCaseWithSisMockConnection
{
    #[Test]
    public function it_creates_a_query_to_all_terms(): void
    {
        $sut = new TermsAllRepository(new Term);
        $query = $sut->query();
        $this->assertEquals('select * from [RDS_TERM_VW] where [ACAD_CAREER] <> ? order by [STRM] asc',
            $query->toSql());
        $this->assertEquals('NC', $query->getBindings()[0]);
    }
}
