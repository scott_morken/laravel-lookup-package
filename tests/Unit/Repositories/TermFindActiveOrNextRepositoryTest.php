<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Term;
use Smorken\Lookup\Repositories\TermFindActiveOrNextRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class TermFindActiveOrNextRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_creates_a_query_for_the_active_or_next_term(): void
    {
        Carbon::setTestNow('2024-02-20 10:00:00');
        $this->pdo->expects()
            ->prepare('select top 1 * from [RDS_TERM_VW] where [TERM_END_DT] >= ? order by [STRM] asc')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, '2024-02-20 00:00:00.000', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['STRM' => '1234']]);
        $sut = new TermFindActiveOrNextRepository(new Term);
        $term = $sut();
        $this->assertEquals('1234', $term->id);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }
}
