<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Hcm;
use Smorken\Lookup\Repositories\PersonHcmFindByIdRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonHcmFindByIdRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_finds_using_int_id(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [PS_MC_HCM] where [CAMPUS_ID] = ? order by [LAST_NAME] asc, [FIRST_NAME] asc')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, 1111, 1);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['CAMPUS_ID' => 1111]]);
        $sut = new PersonHcmFindByIdRepository(new Hcm);
        $person = $sut(1111);
        $this->assertEquals(1111, $person->studentId);
    }

    #[Test]
    public function it_finds_using_string_id(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [PS_MC_HCM] where [CAMPUS_ID] = ? order by [LAST_NAME] asc, [FIRST_NAME] asc')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, 1111, 1);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['CAMPUS_ID' => '1111']]);
        $sut = new PersonHcmFindByIdRepository(new Hcm);
        $person = $sut('1111');
        $this->assertEquals('1111', $person->studentId);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
