<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Term;
use Smorken\Lookup\Repositories\TermsActiveAndUpcomingRepository;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class TermsActiveAndUpcomingRepositoryTest extends TestCaseWithSisMockConnection
{
    #[Test]
    public function it_creates_a_query_to_get_active_and_upcoming_terms(): void
    {
        Carbon::setTestNow('2023-09-01 00:00:00');
        $sut = new TermsActiveAndUpcomingRepository(new Term);
        $query = $sut->query();
        $this->assertEquals('select top 15 * from [RDS_TERM_VW] where [TERM_END_DT] >= ? and [ACAD_CAREER] <> ? order by [STRM] asc',
            $query->toSql());
        $this->assertEquals('2023-09-01 00:00:00', (string) $query->getBindings()[0]);
        $this->assertEquals('NC', (string) $query->getBindings()[1]);
        Carbon::setTestNow();
    }
}
