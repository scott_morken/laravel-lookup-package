<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Term;
use Smorken\Lookup\Repositories\TermFindByIdRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class TermFindByIdRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_creates_a_query_using_the_term_identifier(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [RDS_TERM_VW] where [STRM] = ?')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, '1234', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['STRM' => '1234']]);
        $sut = new TermFindByIdRepository(new Term);
        $term = $sut('1234');
        $this->assertEquals('1234', $term->id);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
