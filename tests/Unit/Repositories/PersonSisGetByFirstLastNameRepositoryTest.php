<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Person;
use Smorken\Lookup\Repositories\PersonsSisGetByFirstLastNameRepository;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonSisGetByFirstLastNameRepositoryTest extends TestCaseWithSisMockConnection
{
    #[Test]
    public function it_creates_a_query_without_using_first_and_last_name(): void
    {
        $sut = new PersonsSisGetByFirstLastNameRepository(new Person);
        $query = $sut->query();
        $this->assertEquals('select * from [CDS_PERSON_TBL] order by [PERS_PRIMARY_LAST_NAME] asc, [PERS_PRIMARY_FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals([], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_using_first_and_last_name(): void
    {
        $sut = new PersonsSisGetByFirstLastNameRepository(new Person);
        $sut->addQueryCallback(fn (Builder $query) => $query->nameLike('foo', 'bar'));
        $query = $sut->query();
        $this->assertEquals('select * from [CDS_PERSON_TBL] where [PERS_PRIMARY_LAST_NAME] LIKE ? and ([PERS_PRIMARY_FIRST_NAME] LIKE ? or [PERS_PREFERRED_FIRST_NAME] LIKE ?) order by [PERS_PRIMARY_LAST_NAME] asc, [PERS_PRIMARY_FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals(['foo%', 'bar%', 'bar%'], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_using_last_name(): void
    {
        $sut = new PersonsSisGetByFirstLastNameRepository(new Person);
        $sut->addQueryCallback(fn (Builder $query) => $query->nameLike('foo', null));
        $query = $sut->query();
        $this->assertEquals('select * from [CDS_PERSON_TBL] where [PERS_PRIMARY_LAST_NAME] LIKE ? order by [PERS_PRIMARY_LAST_NAME] asc, [PERS_PRIMARY_FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals(['foo%'], $query->getBindings());
    }
}
