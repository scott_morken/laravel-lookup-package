<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Klass;
use Smorken\Lookup\Repositories\ClassesByClassNameAndTermIdRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class ClassesByClassNameAndTermIdRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_gets_classes_by_class_name_and_term_id(): void
    {
        $this->pdo->expects()
            ->prepare('select count(*) as aggregate from [CDS_CLASS_TBL] where [CLASS_CLASS_NAME] LIKE ? and [CLASS_TERM_CD] = ?')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, 'ABC101%', 2);
        $this->statement->expects()->bindValue(2, '1234', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['aggregate' => 0]]);
        $sut = new ClassesByClassNameAndTermIdRepository(new Klass);
        $classes = $sut('ABC101', '1234');
        $this->assertEmpty($classes);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
