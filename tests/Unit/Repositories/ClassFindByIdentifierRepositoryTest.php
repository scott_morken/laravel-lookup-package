<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Klass;
use Smorken\Lookup\Repositories\ClassFindByIdentifierRepository;
use Smorken\Lookup\Repositories\ClassFindByTermIdAndClassNumberRepository;
use Tests\Smorken\Lookup\Concerns\WithCacheManager;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class ClassFindByIdentifierRepositoryTest extends TestCaseWithSisMockConnection
{
    use WithCacheManager;

    #[Test]
    public function it_finds_a_class_by_identifier(): void
    {
        $this->pdo->expects()
            ->prepare('select top 1 * from [CDS_CLASS_TBL] where [CLASS_TERM_CD] = ? and [CLASS_CLASS_NBR] = ?')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, '1234', 2);
        $this->statement->expects()->bindValue(2, '55555', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([['CLASS_TERM_CD' => '1234', 'CLASS_CLASS_NBR' => '55555']]);
        $this->pdo->expects()
            ->prepare('select [CDS_STU_CLASS_TBL].[ENRL_ACAD_CAREER], [CDS_STU_CLASS_TBL].[ENRL_ACTN_RSN_LAST_CD], [CDS_STU_CLASS_TBL].[ENRL_ACTN_RSN_LAST_LDESC], [CDS_STU_CLASS_TBL].[ENRL_CLASS_NBR], [CDS_STU_CLASS_TBL].[ENRL_EMPLID], [CDS_STU_CLASS_TBL].[ENRL_EARNED_CREDIT], [CDS_STU_CLASS_TBL].[ENRL_GRADE_DATE], [CDS_STU_CLASS_TBL].[ENRL_GRADE_POINTS], [CDS_STU_CLASS_TBL].[ENRL_GRADING_BASIS], [CDS_STU_CLASS_TBL].[ENRL_INCLUDE_IN_GPA], [CDS_STU_CLASS_TBL].[ENRL_INSTITUTION_CD], [CDS_STU_CLASS_TBL].[ENRL_OFFICIAL_GRADE], [CDS_STU_CLASS_TBL].[ENRL_STATUS_REASON_CD], [CDS_STU_CLASS_TBL].[ENRL_TERM_CD], [CDS_STU_CLASS_TBL].[ENRL_UNITS_TAKEN], [CDS_STU_CLASS_TBL].[ENRL_OEE_START_DATE], [CDS_STU_CLASS_TBL].[ENRL_OEE_END_DATE] from [CDS_STU_CLASS_TBL] where (([CDS_STU_CLASS_TBL].[ENRL_TERM_CD] = ? and [CDS_STU_CLASS_TBL].[ENRL_CLASS_NBR] = ?))')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, '1234', 2);
        $this->statement->expects()->bindValue(2, '55555', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([]);
        $this->pdo->expects()
            ->prepare('select [CDS_CLASS_INSTR_TBL].[CLASS_COURSE_ID], [CDS_CLASS_INSTR_TBL].[CLASS_COURSE_OFFER_NBR], [CDS_CLASS_INSTR_TBL].[CLASS_INSTITUTION_CD], [CDS_CLASS_INSTR_TBL].[CLASS_SECTION], [CDS_CLASS_INSTR_TBL].[CLASS_SESSION_CD], [CDS_CLASS_INSTR_TBL].[CLASS_TERM_CD], [CDS_CLASS_INSTR_TBL].[CLASSM_ASSIGN_TYPE], [CDS_CLASS_INSTR_TBL].[CLASSM_CLASS_MTG_NBR], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTR_ASSIGN_SEQ], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTR_LOAD_FACTOR], [CDS_CLASS_INSTR_TBL].[CLASSM_WEEK_WORKLOAD_HRS], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTR_NAME_FLAST], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTR_TYPE], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTRUCTOR_EMPLID], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTRUCTOR_NAME], [CDS_CLASS_INSTR_TBL].[CLASSM_INSTRUCTOR_ROLE] from [CDS_CLASS_INSTR_TBL] where (([CDS_CLASS_INSTR_TBL].[CLASS_COURSE_ID] is null and [CDS_CLASS_INSTR_TBL].[CLASS_COURSE_OFFER_NBR] is null and [CDS_CLASS_INSTR_TBL].[CLASS_TERM_CD] = ? and [CDS_CLASS_INSTR_TBL].[CLASS_SESSION_CD] is null and [CDS_CLASS_INSTR_TBL].[CLASS_SECTION] is null))')
            ->andReturn($this->statement);
        $this->statement->expects()->bindValue(1, '1234', 2);
        $this->statement->expects()->execute()->andReturn(true);
        $this->statement->expects()->fetchAll()->andReturn([]);
        $sut = new ClassFindByIdentifierRepository(new ClassFindByTermIdAndClassNumberRepository(new Klass));
        $klass = $sut('1234:55555');
        $this->assertEquals('1234', $klass->termId);
        $this->assertEquals('55555', $klass->classNumber);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }
}
