<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Person;
use Smorken\Lookup\Repositories\PersonsSisGetByBirthdateRepository;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonSisGetByBirthdateRepositoryTest extends TestCaseWithSisMockConnection
{
    #[Test]
    public function it_creates_a_query_with_a_birthdate(): void
    {
        $sut = new PersonsSisGetByBirthdateRepository(new Person);
        $sut->addQueryCallback(fn (Builder $query) => $query->birthdateIs(Carbon::parse('2000-01-01')));
        $query = $sut->query();
        $this->assertEquals('select * from [CDS_PERSON_TBL] where [PERS_BIRTHDATE] = ? order by [PERS_PRIMARY_LAST_NAME] asc, [PERS_PRIMARY_FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals(['2000-01-01'], $query->getBindings());
    }
}
