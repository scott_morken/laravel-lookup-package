<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Models\Eloquent\Hcm;
use Smorken\Lookup\Repositories\PersonsHcmGetByFirstLastNameRepository;
use Tests\Smorken\Lookup\Unit\TestCaseWithSisMockConnection;

class PersonHmcGetByFirstLastNameRepositoryTest extends TestCaseWithSisMockConnection
{
    #[Test]
    public function it_creates_a_query_using_first_and_last_name(): void
    {
        $sut = new PersonsHcmGetByFirstLastNameRepository(new Hcm);
        $sut->addQueryCallback(fn (Builder $query) => $query->nameLike('foo', 'bar'));
        $query = $sut->query();
        $this->assertEquals('select * from [PS_MC_HCM] where [LAST_NAME] LIKE ? and ([FIRST_NAME] LIKE ? or [MC_PREF_NAME] LIKE ?) order by [LAST_NAME] asc, [FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals(['foo%', 'bar%', 'bar%'], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_using_last_name(): void
    {
        $sut = new PersonsHcmGetByFirstLastNameRepository(new Hcm);
        $sut->addQueryCallback(fn (Builder $query) => $query->nameLike('foo', null));
        $query = $sut->query();
        $this->assertEquals('select * from [PS_MC_HCM] where [LAST_NAME] LIKE ? order by [LAST_NAME] asc, [FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals(['foo%'], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_without_using_first_and_last_name(): void
    {
        $sut = new PersonsHcmGetByFirstLastNameRepository(new Hcm);
        $query = $sut->query();
        $this->assertEquals('select * from [PS_MC_HCM] order by [LAST_NAME] asc, [FIRST_NAME] asc',
            $query->toSql());
        $this->assertEquals([], $query->getBindings());
    }
}
