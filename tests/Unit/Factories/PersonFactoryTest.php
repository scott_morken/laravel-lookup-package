<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Factories;

use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Factories\PersonFactory;
use Smorken\Lookup\Factories\PersonHcmRepositoryFactory;
use Smorken\Lookup\Factories\PersonSisRepositoryFactory;
use Smorken\Lookup\Models\Eloquent\Hcm;
use Smorken\Lookup\Models\Eloquent\Person;

class PersonFactoryTest extends TestCase
{
    protected ?PersonHcmRepositoryFactory $hcmFactory = null;

    protected ?PersonSisRepositoryFactory $sisFactory = null;

    #[Test]
    public function it_retrieves_a_person_using_hcm_and_int_id(): void
    {
        $this->getSisRepositoryFactory()->expects()->find(12345, false)->andReturn(null);
        $hcmPerson = new Hcm;
        $this->getHcmRepositoryFactory()->expects()->find(12345, false)->andReturn($hcmPerson);
        $emptyPerson = m::mock(Person::class);
        $this->getSisRepositoryFactory()->expects()->emptyModel()->andReturn($emptyPerson);
        $emptyPerson->expects()->fromHcmModel($hcmPerson)->andReturn($emptyPerson);
        $sut = $this->getSut();
        $this->assertSame($emptyPerson, $sut->find(12345));
    }

    #[Test]
    public function it_retrieves_a_person_using_hcm_and_string_id(): void
    {
        $this->getSisRepositoryFactory()->expects()->find('12345', false)->andReturn(null);
        $hcmPerson = new Hcm;
        $this->getHcmRepositoryFactory()->expects()->find('12345', false)->andReturn($hcmPerson);
        $emptyPerson = m::mock(Person::class);
        $this->getSisRepositoryFactory()->expects()->emptyModel()->andReturn($emptyPerson);
        $emptyPerson->expects()->fromHcmModel($hcmPerson)->andReturn($emptyPerson);
        $sut = $this->getSut();
        $this->assertSame($emptyPerson, $sut->find('12345'));
    }

    #[Test]
    public function it_retrieves_a_person_using_sis_and_alt_id(): void
    {
        $this->getSisRepositoryFactory()->expects()->findByAltId('abc1234567', false)->andReturn(null);
        $hcmPerson = new Hcm;
        $this->getHcmRepositoryFactory()->expects()->findByAltId('abc1234567', false)->andReturn($hcmPerson);
        $emptyPerson = m::mock(Person::class);
        $this->getSisRepositoryFactory()->expects()->emptyModel()->andReturn($emptyPerson);
        $emptyPerson->expects()->fromHcmModel($hcmPerson)->andReturn($emptyPerson);
        $sut = $this->getSut();
        $this->assertSame($emptyPerson, $sut->find('abc1234567'));
    }

    #[Test]
    public function it_retrieves_a_person_using_sis_and_alt_id_3x7(): void
    {
        $person = new Person;
        $this->getSisRepositoryFactory()->expects()->findByAltId('abc1234567', false)->andReturn($person);
        $sut = $this->getSut();
        $this->assertSame($person, $sut->find('abc1234567'));
    }

    #[Test]
    public function it_retrieves_a_person_using_sis_and_alt_id_5x5(): void
    {
        $person = new Person;
        $this->getSisRepositoryFactory()->expects()->findByAltId('abcde12345', false)->andReturn($person);
        $sut = $this->getSut();
        $this->assertSame($person, $sut->find('abcde12345'));
    }

    #[Test]
    public function it_retrieves_a_person_using_sis_and_int_id(): void
    {
        $person = new Person;
        $this->getSisRepositoryFactory()->expects()->find(12345, false)->andReturn($person);
        $sut = $this->getSut();
        $this->assertSame($person, $sut->find(12345));
    }

    #[Test]
    public function it_retrieves_a_person_using_sis_and_string_id(): void
    {
        $person = new Person;
        $this->getSisRepositoryFactory()->expects()->find('12345', false)->andReturn($person);
        $sut = $this->getSut();
        $this->assertSame($person, $sut->find('12345'));
    }

    #[Test]
    public function it_returns_null_for_an_alt_id(): void
    {
        $this->getSisRepositoryFactory()->expects()->findByAltId('abcde12345', false)->andReturn(null);
        $this->getHcmRepositoryFactory()->expects()->findByAltId('abcde12345', false)->andReturn(null);
        $sut = $this->getSut();
        $this->assertNull($sut->find('abcde12345'));
    }

    #[Test]
    public function it_returns_null_for_an_id(): void
    {
        $this->getSisRepositoryFactory()->expects()->find('12345', false)->andReturn(null);
        $this->getHcmRepositoryFactory()->expects()->find('12345', false)->andReturn(null);
        $sut = $this->getSut();
        $this->assertNull($sut->find('12345'));
    }

    protected function getHcmRepositoryFactory(): PersonHcmRepositoryFactory
    {
        if (! $this->hcmFactory) {
            $this->hcmFactory = m::mock(PersonHcmRepositoryFactory::class);
        }

        return $this->hcmFactory;
    }

    protected function getSisRepositoryFactory(): PersonSisRepositoryFactory
    {
        if (! $this->sisFactory) {
            $this->sisFactory = m::mock(PersonSisRepositoryFactory::class);
        }

        return $this->sisFactory;
    }

    protected function getSut(): PersonFactory
    {
        return new PersonFactory([
            'sis' => $this->getSisRepositoryFactory(), 'hcm' => $this->getHcmRepositoryFactory(),
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
