<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit\Support;

use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Lookup\Models\Eloquent\Hcm;
use Smorken\Lookup\Models\Eloquent\Person;
use Smorken\Lookup\Support\SisHcmPeopleMerge;

class SisHcmPeopleMergeTest extends TestCase
{
    #[Test]
    public function it_merges_hcm_people_into_sis_people(): void
    {
        $sut = new SisHcmPeopleMerge(new Person);
        $sisPeople = new Collection([
            (new Person)->forceFill([
                'PERS_EMPLID' => 100, 'PERS_PRIMARY_FIRST_NAME' => 'First100', 'PERS_PRIMARY_LAST_NAME' => 'Last100',
            ]),
            (new Person)->forceFill([
                'PERS_EMPLID' => 200, 'PERS_PRIMARY_FIRST_NAME' => 'First200', 'PERS_PRIMARY_LAST_NAME' => 'Last200',
            ]),
            (new Person)->forceFill([
                'PERS_EMPLID' => 300, 'PERS_PRIMARY_FIRST_NAME' => 'First300', 'PERS_PRIMARY_LAST_NAME' => 'Last300',
            ]),
        ]);
        $hcmPeople = new Collection([
            (new Hcm)->forceFill(['CAMPUS_ID' => 100, 'FIRST_NAME' => 'First100', 'LAST_NAME' => 'Last100']),
            (new Hcm)->forceFill(['CAMPUS_ID' => 400, 'FIRST_NAME' => 'First400', 'LAST_NAME' => 'Last400']),
        ]);
        $merged = $sut->merge($sisPeople, $hcmPeople);
        $this->assertEquals([
            100 => [
                'id' => 100,
                'PERS_PRIMARY_FIRST_NAME' => 'First100',
                'PERS_PRIMARY_LAST_NAME' => 'Last100',
                'firstName' => 'First100',
                'lastName' => 'Last100',
                'altId' => null,
                'email' => null,
                'phone' => null,
                'dob' => null,
                'hcmId' => null,
                'hcm' => [
                    'studentId' => 100,
                    'FIRST_NAME' => 'First100',
                    'LAST_NAME' => 'Last100',
                    'id' => null,
                    'altId' => null,
                    'statusCode' => null,
                    'firstName' => 'First100',
                    'lastName' => 'Last100',
                    'middleName' => null,
                    'suffix' => null,
                    'previousName' => null,
                    'email' => null,
                    'phone' => null,
                    'primaryLocationCode' => null,
                    'primaryLocationDescription' => null,
                    'facultyCode' => null,
                    'hrStatusCode' => null,
                ],
            ],
            200 => [
                'id' => 200,
                'PERS_PRIMARY_FIRST_NAME' => 'First200',
                'PERS_PRIMARY_LAST_NAME' => 'Last200',
                'firstName' => 'First200',
                'lastName' => 'Last200',
                'altId' => null,
                'email' => null,
                'phone' => null,
                'dob' => null,
                'hcmId' => null,
                'hcm' => null,
            ],
            300 => [
                'id' => 300,
                'PERS_PRIMARY_FIRST_NAME' => 'First300',
                'PERS_PRIMARY_LAST_NAME' => 'Last300',
                'firstName' => 'First300',
                'lastName' => 'Last300',
                'altId' => null,
                'email' => null,
                'phone' => null,
                'dob' => null,
                'hcmId' => null,
                'hcm' => null,
            ],
            400 => [
                'id' => 400,
                'altId' => null,
                'PERS_PRIMARY_FIRST_NAME' => 'First400',
                'PERS_PRIMARY_LAST_NAME' => 'Last400',
                'email' => null,
                'phone' => null,
                'hcmId' => null,
                'firstName' => 'First400',
                'lastName' => 'Last400',
                'dob' => null,
                'hcm' => [
                    'studentId' => 400,
                    'FIRST_NAME' => 'First400',
                    'LAST_NAME' => 'Last400',
                    'id' => null,
                    'altId' => null,
                    'statusCode' => null,
                    'firstName' => 'First400',
                    'lastName' => 'Last400',
                    'middleName' => null,
                    'suffix' => null,
                    'previousName' => null,
                    'email' => null,
                    'phone' => null,
                    'primaryLocationCode' => null,
                    'primaryLocationDescription' => null,
                    'facultyCode' => null,
                    'hrStatusCode' => null,
                ],
            ],
        ], $merged->toArray());
    }
}
