<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Unit;

use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Lookup\Concerns\WithMockConnection;

abstract class TestCaseWithSisMockConnection extends TestCase
{
    use WithMockConnection;

    protected function initRequest(): void
    {
        $container = Container::getInstance();
        $container->bind('request', function () {
            return new Request;
        });
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initSisConnectionResolver();
        $this->initRequest();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
