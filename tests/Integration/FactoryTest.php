<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Integration;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Factories\PersonFactory;
use Smorken\Lookup\Factories\TermRepositoryFactory;
use Smorken\Lookup\Factory;
use Tests\Smorken\Lookup\TestBenchTestCase;

class FactoryTest extends TestBenchTestCase
{
    #[Test]
    public function test_it_can_instantiate_person_factory(): void
    {
        $sut = $this->app[Factory::class];
        $factory = $sut->create(LookupType::PERSON);
        $this->assertInstanceOf(PersonFactory::class, $factory);
    }

    #[Test]
    public function test_it_can_instantiate_term_factory(): void
    {
        $sut = $this->app[Factory::class];
        $factory = $sut->create(LookupType::TERM);
        $this->assertInstanceOf(TermRepositoryFactory::class, $factory);
    }
}
