<?php

declare(strict_types=1);

namespace Tests\Smorken\Lookup\Integration\Repositories;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Lookup\Contracts\Repositories\TermsActiveAndUpcomingRepository;
use Tests\Smorken\Lookup\TestBenchTestCase;

class TermsActiveAndUpcomingRepositoryTest extends TestBenchTestCase
{
    #[Test]
    public function it_instantiates_repository_from_container(): void
    {
        $sut = $this->app[TermsActiveAndUpcomingRepository::class];
        $this->assertInstanceOf(TermsActiveAndUpcomingRepository::class, $sut);
    }
}
