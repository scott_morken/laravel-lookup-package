<?php

namespace Tests\Smorken\Lookup;

use Orchestra\Testbench\TestCase;
use Smorken\Domain\RepositoryServiceProvider;
use Smorken\Lookup\ServiceProvider;

class TestBenchTestCase extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
            RepositoryServiceProvider::class,
        ];
    }
}
